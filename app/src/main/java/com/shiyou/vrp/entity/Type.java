package com.shiyou.vrp.entity;

/**
 * Created by dime on 16/5/6.
 *
 * 类型实体
 */
public class Type {

    private String id;
    private String typeName;

    public Type() {
    }

    public Type(String id, String typeName) {
        this.id = id;
        this.typeName = typeName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "Type{" +
                "id='" + id + '\'' +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
