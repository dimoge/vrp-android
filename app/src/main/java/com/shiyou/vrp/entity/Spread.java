package com.shiyou.vrp.entity;

/**
 * Created by dime on 16/5/10.
 */
public class Spread {

    private String id;
    private String img;
    private String link;
    private String type;

    public Spread() {
    }

    public Spread(String id, String img, String link, String type) {
        this.id = id;
        this.img = img;
        this.link = link;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
