package com.shiyou.vrp.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by dime on 16/4/29.
 * <p/>
 * 信息  统一实体类
 */
public class Video implements Serializable {
    private String id;
    private String title;//标题
    private String detail;//描述
    private String coverUrl;//封面地址
    private String videoUrl;//视频地址
    private Date date;//日期
    private String author;//作者
    private List<String> type;//视频类型
    private int favorite;//点赞
    private int watch;//观看次数
    private int collect;//收藏数
    private int share;//分享数


    public Video() {
    }

    public Video(String id, String title, String detail, String coverUrl, String videoUrl, Date date, String author, List<String> type, int favorite, int watch, int collect, int share) {
        this.id = id;
        this.title = title;
        this.detail = detail;
        this.coverUrl = coverUrl;
        this.videoUrl = videoUrl;
        this.date = date;
        this.author = author;
        this.type = type;
        this.favorite = favorite;
        this.watch = watch;
        this.collect = collect;
        this.share = share;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public int getWatch() {
        return watch;
    }

    public void setWatch(int watch) {
        this.watch = watch;
    }

    public int getCollect() {
        return collect;
    }

    public void setCollect(int collect) {
        this.collect = collect;
    }

    public int getShare() {
        return share;
    }

    public void setShare(int share) {
        this.share = share;
    }
}
