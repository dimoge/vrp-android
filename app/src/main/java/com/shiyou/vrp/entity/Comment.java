package com.shiyou.vrp.entity;

/**
 * Created by dime on 2016/5/26 0026.
 *
 * 评论实体
 */
public class Comment {

    private String id;
    private String uid;
    private String vid;
    private String cid;
    private String userName;
    private String avatar;
    private String content;
    private String create_time;

    public Comment() {
    }

    public Comment(String id, String uid, String vid, String cid, String userName, String avatar, String content, String create_time) {
        this.id = id;
        this.uid = uid;
        this.vid = vid;
        this.cid = cid;
        this.userName = userName;
        this.avatar = avatar;
        this.content = content;
        this.create_time = create_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
