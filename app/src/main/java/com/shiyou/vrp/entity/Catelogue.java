package com.shiyou.vrp.entity;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Created by dime on 2016/5/19 0019.
 * 记录目录
 */
@Table(name = "catelogue")
public class Catelogue {

    @Column(name = "id", isId = true)
    private int id;

    @Column(name = "name", property = "UNIQUE")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
