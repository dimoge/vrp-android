package com.shiyou.vrp.entity;

/**
 * Created by dime on 16/6/13.
 * <p/>
 * 接受错误实体类
 */
public class VrpError {

    private String code;//错误代码
    private String message;//信息

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
