package com.shiyou.vrp.entity;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by dime on 2016/5/18 0018.
 */
@Table(name = "local")
public class Local implements Serializable {
    @Column(name = "id", isId = true)
    private int id;

    @Column(name = "path", property = "UNIQUE")
    private String path;

    @Column(name = "title")
    private String title;

    @Column(name = "img")
    private String img;

    @Column(name = "duration")
    private String duration;

    @Column(name = "date")
    private Date date;

    @Column(name = "parent")
    private String parent;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }
}
