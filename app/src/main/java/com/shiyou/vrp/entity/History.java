package com.shiyou.vrp.entity;

/**
 * Created by dime on 2016/6/2 0002.
 */
public class History {

    private String id;
    private Video video;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }
}
