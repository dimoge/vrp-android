package com.shiyou.vrp.entity;

/**
 * Created by dime on 16/6/3.
 */
public class Recommend {

    private String id;
    private Video video;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }


}
