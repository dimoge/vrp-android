package com.shiyou.vrp;

import android.app.Application;

import com.pgyersdk.crash.PgyCrashManager;
import com.shiyou.vrp.entity.User;

import org.xutils.DbManager;
import org.xutils.x;

/**
 * Created by dime on 16/5/5.
 */
public class App extends Application {

    public static final String SERVER_URL = "http://119.9.68.36/";
    public static final String API_ROOT = "api/";

    public static final String AUTH_TOKEN = "AUTH-TOKEN";//token的key
    public static final String ONLY_WIFI = "ONLY_WIFI";
    public static final String CACHE_PATH = "CACHE_PATH";
    public static final String DOWNLOAD_PATH = "DOWNLOAD_PATH";

    public static User user;

    private static DbManager.DaoConfig daoConfig;


    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.init(this);
        x.Ext.setDebug(false);

        daoConfig = new DbManager.DaoConfig().setDbName("vrp")//数据库名称
                .setDbVersion(1)//数据库版本号
                .setDbUpgradeListener(new DbManager.DbUpgradeListener() {
                    @Override
                    public void onUpgrade(DbManager db, int oldVersion, int newVersion) {
                        //........
                    }
                })//数据库更新工作
                .setDbOpenListener(new DbManager.DbOpenListener() {
                    @Override
                    public void onDbOpened(DbManager db) {
                        db.getDatabase().enableWriteAheadLogging();//开启WAL,提升写入速度
                    }
                });
        //蒲公英上报异常
        PgyCrashManager.register(this);
    }

    public static DbManager.DaoConfig getDaoConfig() {
        return daoConfig;
    }
}
