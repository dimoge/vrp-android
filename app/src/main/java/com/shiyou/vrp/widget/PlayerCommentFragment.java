package com.shiyou.vrp.widget;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.activity.PlayerActivity;
import com.shiyou.vrp.adapter.CommentRecylerAdapter;
import com.shiyou.vrp.entity.Comment;
import com.shiyou.vrp.entity.Video;
import com.shiyou.vrp.event.CommentSecondAddEvent;
import com.shiyou.vrp.utils.HttpUtils;
import com.shiyou.vrp.utils.SharedPreferencesUtils;
import com.shiyou.vrp.utils.Utils;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dime on 2016/5/27 0027.
 */
@ContentView(R.layout.fragment_player_comment)
public class PlayerCommentFragment extends BaseFragment {

    private Video video;

    private Gson gson = new Gson();

    @ViewInject(R.id.fragment_comment_root_view)
    private LinearLayout rootView;

    @ViewInject(R.id.item_comment_input_lyt)
    private LinearLayout commentLyt;
    @ViewInject(R.id.item_comment_input_cimg)
    private CircleImageView headImg;
    @ViewInject(R.id.item_comment_input_content_edit)
    private EditText contentEdit;
    @ViewInject(R.id.item_comment_input_btn)
    private Button submitBtn;

    @ViewInject(R.id.item_comment_second_input_lyt)
    private LinearLayout commentSecondLyt;
    @ViewInject(R.id.item_comment_second_avatar_cimg)
    private CircleImageView sndHeadImg;
    @ViewInject(R.id.item_comment_second_input_content_edit)
    private EditText sndContentEdit;
    @ViewInject(R.id.item_comment_second_input_btn)
    private Button sndSubmitBtn;

    @ViewInject(R.id.recyler_view)
    private RecyclerView recyclerView;

    private List<Comment> commentList = new ArrayList<>();
    private CommentRecylerAdapter commentRecylerAdapter;

    CommentSecondAddEvent event;

    public PlayerCommentFragment() {
    }

    public PlayerCommentFragment(Video video) {
        this.video = video;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        KeyboardVisibilityEvent.setEventListener(getActivity(), new KeyboardVisibilityEventListener() {
            @Override
            public void onVisibilityChanged(boolean isOpen) {
                if (!isOpen) {
                    contentEdit.setText("");

                    commentSecondLyt.setVisibility(View.GONE);
                    commentLyt.setVisibility(View.VISIBLE);
                }
            }
        });


        //初始化输入框的头像
        if (App.user != null) {
            HttpUtils.loadCimg(App.user.getAvatarUrl(), headImg);
        }
        //初始化一级评论
        getCommentHttp(video.getId());
    }

    /**
     * 添加一级评论
     *
     * @param view
     */
    @Event(value = R.id.item_comment_input_btn)
    private void addComment(View view) {
        if (TextUtils.isEmpty(contentEdit.getText())) return;
        String content = contentEdit.getText().toString().trim();
        if (content.equals("")) return;
        String authToken = SharedPreferencesUtils.getString(App.AUTH_TOKEN);
        if (authToken.equals("")) {
            Toast.makeText(x.app(), "请先登录", Toast.LENGTH_LONG).show();
            return;
        }
        addCommentHttp(content, video.getId(), authToken);
    }

    /**
     * 添加二级评论
     *
     * @param view
     */
    @Event(value = R.id.item_comment_second_input_btn)
    private void addSecondComment(View view) {
        if (TextUtils.isEmpty(sndContentEdit.getText())) return;
        String content = sndContentEdit.getText().toString().trim();
        if (content.equals("")) return;
        String authToken = SharedPreferencesUtils.getString(App.AUTH_TOKEN);
        if (authToken.equals("")) {
            Toast.makeText(x.app(), "请先登录", Toast.LENGTH_LONG).show();
            return;
        }
        addSecondCommentHttp(content, video.getId(), event.getCid(), authToken);
    }

    @Subscribe
    public void onMessageEvent(CommentSecondAddEvent event) {
        this.event = event;
        commentLyt.setVisibility(View.INVISIBLE);
        commentSecondLyt.setVisibility(View.VISIBLE);
        sndContentEdit.setHint("回复@" + event.getUserName() + ":");
        sndContentEdit.setText("");
    }

    /**
     * 添加一级评论
     *
     * @param content 评论内容
     * @param vid     视频id
     */

    private void addCommentHttp(final String content, String vid, String authToken) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "comment/add.ac");
        requestParams.addBodyParameter("content", content);
        requestParams.addBodyParameter("vid", vid);
        requestParams.addHeader(App.AUTH_TOKEN, authToken);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(x.app(), "评论成功", Toast.LENGTH_SHORT).show();
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                //将新的一级评论添加到评论列表中去
                if (commentRecylerAdapter != null) {
                    Comment comment = new Comment();
                    comment.setId(result);
                    comment.setContent(content);
                    comment.setCreate_time(Utils.date2String(new Date()));
                    if (App.user != null) {
                        comment.setUserName(App.user.getNikeName());
                        comment.setAvatar(App.user.getAvatarUrl());
                    }
                    commentList.add(0, comment);
                    commentRecylerAdapter.updateData(commentList);
                }

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 添加二级评论
     *
     * @param content 评论内容
     * @param vid     视频id
     * @param cid     一级评论id
     */
    private void addSecondCommentHttp(final String content, final String vid, final String cid, String authToken) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "secondComment/add.ac");
        requestParams.addBodyParameter("content", content);
        requestParams.addBodyParameter("vid", vid);
        requestParams.addBodyParameter("cid", cid);
        requestParams.addHeader(App.AUTH_TOKEN, authToken);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(x.app(), "回复成功!", Toast.LENGTH_SHORT).show();
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
                        .hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                sndContentEdit.clearFocus();
                sndContentEdit.setText("");
                View view = LayoutInflater.from(getContext()).inflate(R.layout.item_comment_second, null, true);
                CommentRecylerAdapter.CommentSecondHolder holder = commentRecylerAdapter.new CommentSecondHolder(view);
                if (App.user != null) {
                    HttpUtils.loadCimg(App.user.getAvatarUrl(), holder.headImg);
                    holder.nickNameTv.setText(App.user.getNikeName());
                }
                holder.contentTv.setText(content);
                holder.dateTv.setText(Utils.date2String(new Date()));
                event.getLyt().addView(view, 0);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 获取视频的一级评论
     *
     * @param vid 视频id
     */
    private void getCommentHttp(final String vid) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "comment/getComment.ac");
        requestParams.addBodyParameter("vid", vid);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                commentList.clear();
                commentList = gson.fromJson(result, new TypeToken<List<Comment>>() {
                }.getType());
                recyclerView.setLayoutManager(new LinearLayoutManager(x.app()));
                commentRecylerAdapter = new CommentRecylerAdapter(x.app(), commentList, video);
                recyclerView.setAdapter(commentRecylerAdapter);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 获取二级评论
     *
     * @param cid 一级评论id
     */
    private void getSecondComment(String cid) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "secondComment/getSecondComment.ac");
        requestParams.addBodyParameter("cid", cid);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    public EditText getContentEdit() {
        return contentEdit;
    }

    public void setContentEdit(EditText contentEdit) {
        this.contentEdit = contentEdit;
    }
}
