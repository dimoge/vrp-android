package com.shiyou.vrp.widget;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.felipecsl.gifimageview.library.GifImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.activity.DownloadActivity;
import com.shiyou.vrp.activity.SearchActivity;
import com.shiyou.vrp.adapter.FragmentGreatPagerAdapter;
import com.shiyou.vrp.entity.Type;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.image.ImageOptions;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dime on 2016/4/28 0028.
 */
@ContentView(R.layout.fragment_great)
public class GreatFragment extends BaseFragment {

    @ViewInject(R.id.fragment_great_viewpager)
    private ViewPager viewPager;
    @ViewInject(R.id.fragment_great_tablayout)
    private TabLayout tabLayout;
    @ViewInject(R.id.network_error_lyt)
    private LinearLayout errorLyt;
    @ViewInject(R.id.network_loading_lyt)
    private LinearLayout loadingLyt;
    @ViewInject(R.id.network_loading_img)
    private GifImageView netWorkLoadingImg;
    @ViewInject(R.id.network_reconn_btn)
    private Button reconnBtn;//断线重连

    FragmentGreatPagerAdapter greatAdapter;//pagerAdapter

    private List<Fragment> fragmentList = new ArrayList<>();
    private List<String> titleList = new ArrayList<>();
    private List<Type> typeList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        Log.e("greatFragment", "onCreatView");
//        View view = inflater.inflate(R.layout.fragment_great, container, false);
//
//        initView(view);
//
//        return view;
//    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            InputStream inputStream = getResources().getAssets().open("network_loading.gif");
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);
            netWorkLoadingImg.setBytes(bytes);
            netWorkLoadingImg.startAnimation();

        } catch (IOException e) {
            e.printStackTrace();
        }
        getTypes();
    }

    /**
     * 获取所有视频分类
     */
    private void getTypes() {
        RequestParams params = new RequestParams(App.SERVER_URL + App.API_ROOT + "video/getTypes.ac");
        x.http().post(params, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                loadingLyt.setVisibility(View.INVISIBLE);//加载成功, 隐藏loading动画
                netWorkLoadingImg.stopAnimation();
                Gson gson = new Gson();
                typeList = gson.fromJson(result, new TypeToken<List<Type>>() {
                }.getType());
                initPager();
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                //网络错误, 显示错误提示
                loadingLyt.setVisibility(View.INVISIBLE);
                errorLyt.setVisibility(View.VISIBLE);
            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 断线重连
     */
    @Event(value = R.id.network_reconn_btn)
    private void reconn(View view) {
        getTypes();
        errorLyt.setVisibility(View.INVISIBLE);
        loadingLyt.setVisibility(View.VISIBLE);
    }

    @Event(value = R.id.great_fragment_search_imgbtn)
    private void go2Search(View view) {
        startActivity(new Intent(getContext(), SearchActivity.class));
    }

    @Event(value = R.id.great_fragment_download_imgbtn)
    private void go2Download(View view){
        startActivity(new Intent(getContext(), DownloadActivity.class));
    }

    private void initPager() {
        if (greatAdapter == null) {
            Type type = new Type("0", "精选");
            typeList.add(0, type);//type组装完成

            for (int i = 0; i < typeList.size(); i++) {
                if (i == 0) {
                    GreatTypeOneFragment greatTypeOneFragment = new GreatTypeOneFragment();
                    fragmentList.add(greatTypeOneFragment);
                } else {
                    GreatTypeCommentFragment greatTypeCommentFragment = new GreatTypeCommentFragment(typeList.get(i));
                    fragmentList.add(greatTypeCommentFragment);
                }
            }
            greatAdapter = new FragmentGreatPagerAdapter(getActivity().getSupportFragmentManager(), x.app(), fragmentList, typeList);
            viewPager.setAdapter(greatAdapter);
            tabLayout.setupWithViewPager(viewPager);
            /**
             *  警告!警告!警告!
             * 这里只是临时措施, 为了防止viewpager切换时销毁Fragment导致重复加载
             * 等待被优化
             */
            viewPager.setOffscreenPageLimit(20);
        } else {
            greatAdapter.updateData(typeList);
        }
    }

}
