package com.shiyou.vrp.widget;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.activity.FileActivity;
import com.shiyou.vrp.adapter.LocalCatelogueAdapter;
import com.shiyou.vrp.adapter.LocalRecentAdapter;
import com.shiyou.vrp.entity.Catelogue;
import com.shiyou.vrp.entity.Local;

import org.xutils.DbManager;
import org.xutils.ex.DbException;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by dime on 2016/4/28 0028.
 */
@ContentView(R.layout.fragment_local)
public class LocalFragment extends BaseFragment {

    boolean TAG = false;

    private final int MAX_RECENT = 3;

    @ViewInject(R.id.local_no_import_tv)
    private TextView noImportTv;
    @ViewInject(R.id.local_no_recent_tv)
    private TextView noRecentTv;

    @ViewInject(R.id.local_recylerview_recent)
    private RecyclerView recentRecylerView;
    @ViewInject(R.id.local_recyler_catelogue)
    private RecyclerView catelogueRecylerView;

    private LocalRecentAdapter recentAdapter;
    private List<Local> recentList = new ArrayList<>();//最近的数据源

    private List<Catelogue> catelogueList = new ArrayList<>();//目录数据源
    private LocalCatelogueAdapter catelogueAdapter;

    private DbManager dbManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_local, container, false);
//        return view;
//    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (dbManager == null) {
            dbManager = x.getDb(App.getDaoConfig());
        }
    }

    @Event(value = R.id.local_import)
    private void importVideo(View view) {
        startActivity(new Intent(x.app(), FileActivity.class));
    }

    @Override
    public void onResume() {
        super.onResume();

        if (dbManager == null) {
            dbManager = x.getDb(App.getDaoConfig());
        }
        try {
            recentList = dbManager.selector(Local.class).orderBy("date", true).limit(MAX_RECENT).findAll();
            catelogueList = dbManager.findAll(Catelogue.class);

            noRecentTv.setVisibility(View.GONE);
            noImportTv.setVisibility(View.GONE);

        } catch (DbException e) {
            e.printStackTrace();
        }finally {
            if(recentList == null){
                recentList = new ArrayList<>();
                noRecentTv.setVisibility(View.VISIBLE);
            } else if (recentList.size() == 0) {
                noRecentTv.setVisibility(View.VISIBLE);
            }
            if(catelogueList == null){
                catelogueList = new ArrayList<>();
                noImportTv.setVisibility(View.VISIBLE);
            } else if (catelogueList.size() == 0) {
                noImportTv.setVisibility(View.VISIBLE);
            }
        }

        if (recentAdapter == null) {
            //第一次进入界面,初始化界面
            recentAdapter = new LocalRecentAdapter(x.app(), recentList);
            recentRecylerView.setAdapter(recentAdapter);
            recentRecylerView.setLayoutManager(new LinearLayoutManager(x.app()));
        } else {
            //界面已经完成, 那么就刷新数据吧, 走你....~~~~~~~
            recentAdapter.updateData(recentList);
        }

        if (catelogueAdapter == null) {
            //同上
            catelogueAdapter = new LocalCatelogueAdapter(x.app(), catelogueList);
            catelogueRecylerView.setLayoutManager(new LinearLayoutManager(x.app()));
            catelogueRecylerView.setAdapter(catelogueAdapter);
        } else {
            catelogueAdapter.updateDate(catelogueList);
        }
    }

    /**
     * 将数据按照时间先后排序
     *
     * @return
     */
    private List<Local> filterRecent() {
        recentList.clear();
        for (Local local : recentList) {
            if (local.getDate().getTime() > new Date(0).getTime()) {
                recentList.add(local);
            }
        }
        Collections.sort(recentList, new Comparator<Local>() {
            @Override
            public int compare(Local local, Local t1) {
                return local.getDate().compareTo(t1.getDate());
            }
        });
        return recentList;
    }
}
