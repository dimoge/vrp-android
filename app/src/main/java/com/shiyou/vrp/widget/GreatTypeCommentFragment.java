package com.shiyou.vrp.widget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.adapter.FragmentTypeCommentRecylerViewAdapter;
import com.shiyou.vrp.entity.Type;
import com.shiyou.vrp.entity.Video;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dime on 16/5/6.
 */
@ContentView(R.layout.fragment_great_type_comment)
public class GreatTypeCommentFragment extends BaseFragment {

    private Type type;

    @ViewInject(R.id.pull_refresh_layout)
    private PullRefreshLayout pullRefreshLayout;
    @ViewInject(R.id.fragment_great_type_comment_recylerview)
    private RecyclerView recyclerView;

    private List<Video> videoList = new ArrayList<>();

    public GreatTypeCommentFragment() {
    }

    public GreatTypeCommentFragment(Type type) {
        this.type = type;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.getString("TYPE_ID") != null) {
                type = new Type();
                type.setId(savedInstanceState.getString("TYPE_ID"));
            }
        }

        getVideosByTpye();
        pullRefreshLayout.setEnabled(false);
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getVideosByTpye();
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //TODO:懒加载:用户可见时加载数据
    }

    private void getVideosByTpye() {
        final RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "video/getVideosByType.ac");
        requestParams.addBodyParameter("id", type.getId());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                videoList = gson.fromJson(result, new TypeToken<List<Video>>() {
                }.getType());
                //这里的context很有意思
                FragmentTypeCommentRecylerViewAdapter fragmentTypeCommentRecylerViewAdapter = new FragmentTypeCommentRecylerViewAdapter(getContext(), videoList);
                recyclerView.setLayoutManager(new GridLayoutManager(x.app(), 2));
                recyclerView.setAdapter(fragmentTypeCommentRecylerViewAdapter);
                pullRefreshLayout.setEnabled(true);
                pullRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {  

            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在fragment销毁时调用
        if (type != null) {
            outState.putSerializable("TYPE_ID", type.getId());
        }
    }
}
