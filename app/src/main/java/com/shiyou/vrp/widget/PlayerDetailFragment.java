package com.shiyou.vrp.widget;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.download.DownloadInfo;
import com.shiyou.vrp.download.DownloadManager;
import com.shiyou.vrp.entity.Video;
import com.shiyou.vrp.utils.SharedPreferencesUtils;

import org.xutils.common.Callback;
import org.xutils.ex.DbException;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.HashMap;
import java.util.List;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

/**
 * Created by dime on 2016/5/27 0027.
 */
@ContentView(R.layout.fragment_player_detail)
public class PlayerDetailFragment extends BaseFragment {

    private Video video;

    OnekeyShare oks;

    //标题部分
    @ViewInject(R.id.player_video_title_lyt)
    private LinearLayout videoTitleLyt;
    @ViewInject(R.id.player_video_title_tv)
    private TextView videoTitleTv;              //显示标题
    @ViewInject(R.id.player_video_detail_tv)
    private TextView videoDetailTv;             //显示详细
    @ViewInject(R.id.player_video_watch_tv)
    private TextView videoWatchTv;              //显示视频观看数
    @ViewInject(R.id.player_video_expand_tg)
    private ToggleButton videoDetailTg;          //tb按钮,用于展开
    @ViewInject(R.id.player_expand_detail_lyt)
    private ExpandableRelativeLayout videoDetailElyt;//可展开空间
    @ViewInject(R.id.player_video_share_tv)
    private TextView videoShareTv;              //分享数
    @ViewInject(R.id.player_share_imgbtn)
    private ImageButton videoShareImgbtn;       //分享按钮
    @ViewInject(R.id.player_download_imgbtn)
    private ImageButton videoDownloadImgbtn;    //下载按钮
    @ViewInject(R.id.player_video_collect_imgbtn)
    private ImageView videoCollectBtn;
    @ViewInject(R.id.player_video_collect_tv)
    private TextView videoCollectTv;//收藏数

    private boolean isCollect = false;
    private boolean isFavroite = false;

    public PlayerDetailFragment() {
    }

    public PlayerDetailFragment(Video video) {
        this.video = video;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        videoTitleTv.setText(video.getTitle());
        videoDetailTv.setText(video.getDetail());
        videoShareTv.setText(video.getShare() + "");
        videoWatchTv.setText(video.getWatch() + " 次观看");
        videoCollectTv.setText(video.getCollect() + "");
        addWatchHttp();//观看数自增
        if (!SharedPreferencesUtils.getString(App.AUTH_TOKEN).equals("")) {
            addHistoryHttp();
            isCollected(video.getId());
        }


    }

    /**
     * 点击展开空间
     *
     * @param view
     */
    @Event(value = R.id.player_video_title_lyt)
    private void expandDone(View view) {
        if (videoDetailTg.isChecked()) {
            videoDetailElyt.expand();
            videoDetailTg.setChecked(false);
        } else {
            videoDetailElyt.collapse();
            videoDetailTg.setChecked(true);
        }
    }

    /**
     * 下载
     */
    @Event(value = R.id.player_download_imgbtn)
    private void download(View view) {
        if (SharedPreferencesUtils.getString(App.AUTH_TOKEN).equals("")) {
            Toast.makeText(x.app(), "请登录....", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            DownloadManager.getInstance().startDownload(
                    video.getVideoUrl(),
                    video.getCoverUrl(),
                    video.getTitle(),
                    SharedPreferencesUtils.getString(App.DOWNLOAD_PATH) + video.getTitle() + ".mp4",
                    true,
                    false,
                    null
            );
            Toast.makeText(x.app(), "开始下载:" + video.getTitle(), Toast.LENGTH_LONG).show();
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    /**
     * 视频是否收藏
     *
     * @param vid
     */
    private void isCollected(String vid) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "collection/isCollection.ac");
        requestParams.addHeader(App.AUTH_TOKEN, SharedPreferencesUtils.getString(App.AUTH_TOKEN));
        requestParams.addBodyParameter("vid", vid);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                if (result.contains("true")) {
                    isCollect = true;
                    videoCollectBtn.setBackgroundResource(R.drawable.player_collect_yes_bg);//显示已经收藏图标
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 分享事件
     *
     * @param v
     */
    @Event(value = R.id.player_share_imgbtn)
    private void share(View v) {
        ShareSDK.initSDK(x.app());
        oks = new OnekeyShare();

        oks.setCallback(platformActionListener);

        oks.setTitle("oh分享-" + video.getTitle());       //title是标题, 所有平台通用
        oks.setText(video.getDetail());                 //text是内容, 所有平台通用
        oks.setImageUrl(video.getCoverUrl());            //imgUrl是图片, 显示分享的图片

        oks.setTitleUrl(App.SERVER_URL + "share?vid=" + video.getId());// titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        oks.setUrl(App.SERVER_URL + "share?vid=" + video.getId());// url仅在微信（包括好友和朋友圈）中使用

        oks.show(x.app());
    }


    @Event(value = R.id.player_video_collect_imgbtn)
    private void collect(View view) {
        if (SharedPreferencesUtils.getString(App.AUTH_TOKEN).equals("")) {
            Toast.makeText(x.app(), "请登录....", Toast.LENGTH_SHORT).show();
            return;
        }
        if (isCollect) {//取消收藏
            removeCollectHttp();
        } else {
            addCollcetHttp();
        }
    }

    /**
     * 观看数自增加 网络请求
     */
    private void addWatchHttp() {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "video/addWatchCount.ac");
        requestParams.addBodyParameter("id", video.getId());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                video.setWatch(video.getWatch() + 1);
                videoWatchTv.setText(video.getWatch() + "次观看");
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 添加播放历史记录
     */
    private void addHistoryHttp() {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "history/add.ac");
        requestParams.addHeader(App.AUTH_TOKEN, SharedPreferencesUtils.getString(App.AUTH_TOKEN));
        requestParams.addBodyParameter("vid", video.getId());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 分享数自增 网络请求
     */
    private void addShareHttp() {
        if (SharedPreferencesUtils.getString(App.AUTH_TOKEN).equals("")) return;
        String authToken = SharedPreferencesUtils.getString(App.AUTH_TOKEN);
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "video/addShareCount.ac");
        requestParams.addHeader(App.AUTH_TOKEN, authToken);
        requestParams.addBodyParameter("id", video.getId());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                video.setShare(video.getShare() + 1);
                videoShareTv.setText(video.getShare() + "");
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 收藏视频 http请求
     */
    private void addCollcetHttp() {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "collection/add.ac");
        requestParams.addBodyParameter("vid", video.getId());
        requestParams.addHeader(App.AUTH_TOKEN, SharedPreferencesUtils.getString(App.AUTH_TOKEN));
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(x.app(), "收藏成功", Toast.LENGTH_SHORT).show();
                video.setCollect(video.getCollect() + 1);
                videoCollectTv.setText(video.getCollect() + "");
                isCollect = true;
                videoCollectBtn.setBackgroundResource(android.R.color.transparent);
                videoCollectBtn.setBackgroundResource(R.drawable.player_collect_yes_bg);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Toast.makeText(x.app(), "已加入收藏列表", Toast.LENGTH_SHORT).show();
                isCollect = true;
                videoCollectBtn.setBackgroundResource(R.drawable.player_collect_yes_bg);
            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 取消收藏 http请求
     */
    private void removeCollectHttp() {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "collection/remove.ac");
        requestParams.addHeader(App.AUTH_TOKEN, SharedPreferencesUtils.getString(App.AUTH_TOKEN));
        requestParams.addBodyParameter("vid", video.getId());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                isCollect = false;
                Toast.makeText(x.app(), "取消收藏", Toast.LENGTH_SHORT).show();
                videoCollectBtn.setBackgroundResource(android.R.color.transparent);
                videoCollectBtn.setImageResource(R.drawable.player_collect_no_bg);
                video.setCollect(video.getCollect() - 1);
                videoCollectTv.setText(video.getCollect() + "");
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * ShareSDK分享 回调
     */
    PlatformActionListener platformActionListener = new PlatformActionListener() {
        @Override
        public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
            Toast.makeText(x.app(), "分享 +1", Toast.LENGTH_SHORT).show();
            addShareHttp();
        }

        @Override
        public void onError(Platform platform, int i, Throwable throwable) {
            Toast.makeText(x.app(), "分享失败", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel(Platform platform, int i) {
            Toast.makeText(x.app(), "分享取消", Toast.LENGTH_SHORT).show();
        }
    };
}
