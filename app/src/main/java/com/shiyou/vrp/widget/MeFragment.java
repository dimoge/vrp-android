package com.shiyou.vrp.widget;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.activity.CollectListActivity;
import com.shiyou.vrp.activity.HistoryListActivity;
import com.shiyou.vrp.activity.LoginActivity;
import com.shiyou.vrp.activity.SetupActivity;
import com.shiyou.vrp.activity.ShareListActivity;
import com.shiyou.vrp.activity.UserUpdateActivity;
import com.shiyou.vrp.entity.User;
import com.shiyou.vrp.event.AvatarEvent;
import com.shiyou.vrp.event.GenderEvent;
import com.shiyou.vrp.event.NickNameEvent;
import com.shiyou.vrp.utils.SharedPreferencesUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.xutils.common.Callback;
import org.xutils.common.util.DensityUtil;
import org.xutils.http.RequestParams;
import org.xutils.image.ImageOptions;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dime on 2016/4/28 0028.
 */
@ContentView(R.layout.fragment_me)
public class MeFragment extends BaseFragment {

    @ViewInject(R.id.me_email_lyt)
    private LinearLayout me_email_lyt;
    @ViewInject(R.id.me_score_lyt)
    private LinearLayout me_score_lyt;
    @ViewInject(R.id.me_user_head_img)
    private CircleImageView headImg;
    @ViewInject(R.id.me_user_nikename_tv)
    private TextView nikeNameTv;
    @ViewInject(R.id.me_user_email_tv)
    private TextView emailTv;
    @ViewInject(R.id.me_user_score_tv)
    private TextView scoreTv;
    @ViewInject(R.id.me_is_valid_lyt)
    private LinearLayout isValidLyt;
    @ViewInject(R.id.me_info_update_imgbtn)
    private ImageButton updateBtn;
    @ViewInject(R.id.me_collect_lyt)
    private LinearLayout colletcLyt;
    @ViewInject(R.id.me_history_lyt)
    private LinearLayout historyLyt;
    @ViewInject(R.id.me_share_lyt)
    private LinearLayout shareLyt;

    private User user;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_me, container, false);
//        x.view().inject(getView());
//        initView(view);
//        return view;
//    }


    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);

        if (user != null) {
            initView(user);
        } else {
            String authToken = SharedPreferencesUtils.getString("AUTH-TOKEN");
            if (authToken.equals("")) {
                Toast.makeText(x.app(), "请登录!!!", Toast.LENGTH_LONG).show();
                return;
            }
            getInfoHttp(authToken);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onStop() {
        super.onStop();
//        if (EventBus.getDefault().isRegistered(this)) EventBus.getDefault().unregister(this);
    }

    @Subscribe
    void onMessageEvent(NickNameEvent nickNameEvent) {
        user.setNikeName(nickNameEvent.getName());
    }

    @Subscribe
    public void onMessageEvent(AvatarEvent avatarEvent) {
        user.setAvatarUrl(avatarEvent.getAvatarUrl());
//        loadAvatar(user.getAvatarUrl(), headImg);
    }

    @Subscribe
    public void onMessageEvent(GenderEvent genderEvent) {
        user.setGender(genderEvent.getGender());
    }

    /**
     * 跳转到设置界面
     *
     * @param view
     */
    @Event(value = R.id.me_setup_lyt)
    private void go2SetUpActivity(View view) {
        startActivity(new Intent(getContext(), SetupActivity.class));
    }

    /**
     * 跳转到修改界面
     */
    @Event(value = R.id.me_info_update_imgbtn)
    private void go2updateActivity(View view) {
        if (user == null) {
            Toast.makeText(x.app(), "未登陆....", Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(x.app(), UserUpdateActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("USER", user);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    /**
     * 跳转到设置界面
     */
    @Event(value = R.id.me_setup_lyt)
    private void go2SetupActivity(View view) {
        startActivity(new Intent(x.app(), SetupActivity.class));
    }

    /**
     * 跳转到登录界面
     *
     * @param view
     */
    @Event(value = R.id.me_user_login_lyt)
    private void go2Login(View view) {
        if (user == null) {
            startActivity(new Intent(getContext(), LoginActivity.class));
            getActivity().finish();
        }
    }

    /**
     * 跳转到收藏界面
     *
     * @param view
     */
    @Event(value = R.id.me_collect_lyt)
    private void go2CollectListActivity(View view) {
        startActivity(new Intent(x.app(), CollectListActivity.class));
    }

    /**
     * 跳转到历史界面
     *
     * @param view
     */
    @Event(value = R.id.me_history_lyt)
    private void go2HistoryListActivity(View view) {
        startActivity(new Intent(x.app(), HistoryListActivity.class));
    }

    /**
     * 跳转到分享界面
     *
     * @param view
     */
    @Event(value = R.id.me_share_lyt)
    private void go2ShareListActivity(View view) {
        startActivity(new Intent(x.app(), ShareListActivity.class));
    }

    /**
     * 验证用户身份
     *
     * @param view
     */
    @Event(value = R.id.me_is_valid_lyt)
    private void doValid(View view) {
        if (SharedPreferencesUtils.getString(App.AUTH_TOKEN).equals("")) {
            Toast.makeText(x.app(), "请登录!!!", Toast.LENGTH_LONG).show();
            return;
        }
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/sendEmail.ac");
        requestParams.addBodyParameter("uid", user.getId());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("提示");
                builder.setMessage("激活邮件已经发送到您的邮箱:" + user.getEmail() + "中, 请查收!!!");
                builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.create().show();

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });

    }

    /**
     * 网络请求 : 获取用户数据
     *
     * @param authtoken
     */
    private void getInfoHttp(String authtoken) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/getProfile.ac");
        requestParams.addHeader("AUTH-TOKEN", authtoken);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                user = gson.fromJson(result, new TypeToken<User>() {
                }.getType());
                me_email_lyt.setVisibility(View.VISIBLE);
                me_score_lyt.setVisibility(View.VISIBLE);
                initView(user);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    private void initView(User user) {
        nikeNameTv.setText(user.getNikeName());
        emailTv.setText(user.getEmail());
        scoreTv.setText(user.getScore() + "");
        if (user.isVerification()) {
            isValidLyt.setVisibility(View.INVISIBLE);
        } else {
            isValidLyt.setVisibility(View.VISIBLE);
        }
        loadAvatar(user.getAvatarUrl(), headImg);
    }

    /**
     * 加载头像方法封装
     *
     * @param avatarUrl
     * @param view
     */
    private void loadAvatar(String avatarUrl, final CircleImageView view) {
        ImageOptions imageOptions = new ImageOptions.Builder()
                .setSize(DensityUtil.dip2px(80), DensityUtil.dip2px(80))
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .build();
        x.image().loadDrawable(avatarUrl, imageOptions, new Callback.CommonCallback<Drawable>() {
            @Override
            public void onSuccess(Drawable result) {
                view.setImageBitmap(((BitmapDrawable) result).getBitmap());
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }
}
