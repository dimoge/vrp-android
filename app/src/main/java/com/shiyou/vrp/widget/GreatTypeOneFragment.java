package com.shiyou.vrp.widget;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.activity.WebActivity;
import com.shiyou.vrp.adapter.FragmentTypeCommentRecylerViewAdapter;
import com.shiyou.vrp.adapter.FragmentTypeOneSpreadAdapter;
import com.shiyou.vrp.entity.Recommend;
import com.shiyou.vrp.entity.Spread;
import com.shiyou.vrp.entity.Video;
import com.shiyou.vrp.activity.PlayerActivity;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dime on 16/4/29.
 * 第一个fragment
 * 第一个小fragmet
 */
@ContentView(R.layout.fragment_great_type_one)
public class GreatTypeOneFragment extends BaseFragment {

    @ViewInject(R.id.pull_refresh_layout)
    private PullRefreshLayout pullRefreshLayout;

    @ViewInject(R.id.fragment_great_typeone_viewpager)
    private ViewPager viewPager;
    @ViewInject(R.id.fragment_great_typeone_guide_lyt)
    private LinearLayout guideLyt;

    @ViewInject(R.id.fragment_great_type_one_newest_recylerview)
    private RecyclerView newestRecylerView;
    @ViewInject(R.id.fragment_great_type_one_recommend_recylerview)
    private RecyclerView recommendRecylerView;
    @ViewInject(R.id.fragment_great_type_one_recommend_history_recylerview)
    private RecyclerView recommendHistoryRecylerView;

    List<ImageView> imgViewList = new ArrayList<>();
    Map<String, List<Video>> greatVideoMap = new HashMap<>();

    List<Spread> spreadList = new ArrayList<>();
    List<Video> videoList4Newest = new ArrayList<>();
    List<Video> VideoList4RecommendHistory = new ArrayList<>();
    List<Video> VideoList4RecommendNewest = new ArrayList<>();
    List<Video> videoList4RecommendAll = new ArrayList<>();

    public GreatTypeOneFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_great_type_one, container, false);
//        initView(view);
//        return view;
//    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pullRefreshLayout.setEnabled(false);
        getSpread();//获取头部 推广信息
        getNewest();//获取最新的八条video
        getRecommendAll();//获取推荐
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNewest();//获取最新的八条video
                getRecommendAll();//获取推荐
            }
        });
    }

    /**
     * 获取推广信息
     */
    private void getSpread() {
        final RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "spread/getAll.ac");
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                final Gson gson = new Gson();
                spreadList = gson.fromJson(result, new TypeToken<List<Spread>>() {
                }.getType());

                //如果数据为空, 则不加载viewpager布局
                if (spreadList.size() == 0) {
                    return;
                }

                //这里是ViewPager中的ImageView的 LayoutParams
                ViewGroup.LayoutParams paramsPager = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                //这里是LinearLayout中的ImageView的 LlayoutParams
                LinearLayout.LayoutParams paramsGuide = new LinearLayout.LayoutParams(15, 15);
                paramsGuide.setMargins(10, 0, 10, 0);

                for (final Spread spread : spreadList) {
                    //viewpager
                    final ImageView imgPager = new ImageView(getActivity());
                    imgPager.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    imgPager.setLayoutParams(paramsPager);
                    imgViewList.add(imgPager);
                    if (spread.getType().equals("WEB")) {//跳转到webview
                        imgPager.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(getContext(), WebActivity.class);
                                intent.putExtra("URL", spread.getLink());
                                startActivity(intent);
                            }
                        });
                    } else if (spread.getType().equals("VIDEO")) {
                        RequestParams requestParams1 = new RequestParams(App.SERVER_URL + App.API_ROOT + "video/getVideo.ac");
                        requestParams1.addBodyParameter("id", spread.getLink());
                        x.http().post(requestParams1, new CommonCallback<String>() {
                            @Override
                            public void onSuccess(String result) {
                                final Video video = gson.fromJson(result, new TypeToken<Video>() {
                                }.getType());
                                imgPager.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(getContext(), PlayerActivity.class);
                                        Bundle bundle = new Bundle();
                                        bundle.putSerializable("VIDEO", video);
                                        intent.putExtras(bundle);
                                        startActivity(intent);
                                    }
                                });
                            }

                            @Override
                            public void onError(Throwable ex, boolean isOnCallback) {

                            }

                            @Override
                            public void onCancelled(CancelledException cex) {

                            }

                            @Override
                            public void onFinished() {

                            }
                        });
                    }
                    //LinearLayout
                    ImageView imageView = new ImageView(getActivity());
                    imageView.setBackgroundColor(Color.GRAY);
                    imageView.setLayoutParams(paramsGuide);
                    guideLyt.addView(imageView);
                }
                FragmentTypeOneSpreadAdapter fragmentTypeOneSpreadAdapter = new FragmentTypeOneSpreadAdapter(x.app(), imgViewList, spreadList);
                viewPager.setAdapter(fragmentTypeOneSpreadAdapter);

                guideLyt.getChildAt(0).setBackgroundColor(Color.RED);

                viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    }

                    @Override
                    public void onPageSelected(int position) {
                        //完善导航点的切换
                        for (int i = 0; i < imgViewList.size(); i++) {
                            if (position == i) {
                                guideLyt.getChildAt(i).setBackgroundColor(Color.parseColor("#E62117"));
                            } else {
                                guideLyt.getChildAt(i).setBackgroundColor(Color.parseColor("#FFFFFF"));
                            }
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 获取最新的推荐, 八条数据为限
     */
    private void getNewest() {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "video/getNewest.ac");
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                videoList4Newest = gson.fromJson(result, new TypeToken<List<Video>>() {
                }.getType());
                if (videoList4Newest.size() == 0) {
                    Toast.makeText(x.app(), "暂无数据, 敬请期待!!!", Toast.LENGTH_LONG).show();
                    return;
                }

                GridLayoutManager gridLayoutManager = new GridLayoutManager(x.app(), 2);
                newestRecylerView.setLayoutManager(gridLayoutManager);
                FragmentTypeCommentRecylerViewAdapter fragmentTypeCommentRecylerViewAdapter = new FragmentTypeCommentRecylerViewAdapter(x.app(), videoList4Newest);
                newestRecylerView.setAdapter(fragmentTypeCommentRecylerViewAdapter);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    private void getRecommendAll() {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "recommend/getAll.ac");
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                List<Recommend> recommendList = new ArrayList<Recommend>();
                recommendList = gson.fromJson(result, new TypeToken<List<Recommend>>() {
                }.getType());
                if (recommendList.size() == 0) {
                    Toast.makeText(x.app(), "暂无数据, 敬请期待!!!", Toast.LENGTH_LONG).show();
                    return;
                }

                for (Recommend recommend : recommendList) {
                    videoList4RecommendAll.add(recommend.getVideo());
                }

                //组装推荐 数据
                for (int i = 0; i < videoList4RecommendAll.size(); i++) {
                    if (i <= 3) {
                        VideoList4RecommendNewest.add(videoList4RecommendAll.get(i));//组装最新的四条数据
                    } else {
                        VideoList4RecommendHistory.add(videoList4RecommendAll.get(i));//组装历史数据
                    }
                }
                //展示最新的推荐数据
                FragmentTypeCommentRecylerViewAdapter newestRecoAdapter = new FragmentTypeCommentRecylerViewAdapter(x.app(), VideoList4RecommendNewest);
                recommendRecylerView.setAdapter(newestRecoAdapter);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(x.app(), 2);
                recommendRecylerView.setLayoutManager(gridLayoutManager);
                //展示历史推荐数据
                if (videoList4RecommendAll.size() <= 4) {
                    Toast.makeText(x.app(), "暂无推荐历史数据, 敬请期待", Toast.LENGTH_LONG).show();
                    return;
                }
                FragmentTypeCommentRecylerViewAdapter historyRecoAdapter = new FragmentTypeCommentRecylerViewAdapter(x.app(), VideoList4RecommendHistory);
                recommendHistoryRecylerView.setAdapter(newestRecoAdapter);
                GridLayoutManager gridLayoutManager2 = new GridLayoutManager(x.app(), 2);
                recommendHistoryRecylerView.setLayoutManager(gridLayoutManager2);

                pullRefreshLayout.setEnabled(true);
                pullRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

}
