package com.shiyou.vrp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.entity.User;
import com.shiyou.vrp.utils.SharedPreferencesUtils;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

/**
 * Created by dime on 16/4/29.
 */
public class StartActivity extends Activity {

    Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPreferencesUtils.getBoolean("MAIDEN")) {//是处女吗
                    SharedPreferencesUtils.set(App.CACHE_PATH, Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/Android/data/" + x.app().getPackageName() + "/cache/");
                    SharedPreferencesUtils.set(App.DOWNLOAD_PATH, Environment.getExternalStorageDirectory().getAbsolutePath() +
                            "/Android/data/oh_download/video/");
                    SharedPreferencesUtils.set(App.ONLY_WIFI, true);
                    //跳转到登录
                    startActivity(new Intent(StartActivity.this, LoginActivity.class));
                    SharedPreferencesUtils.set("MAIDEN", false);
                    String authToken = SharedPreferencesUtils.getString(App.AUTH_TOKEN);
                } else {
                    String authToken = SharedPreferencesUtils.getString(App.AUTH_TOKEN);
                    if (!authToken.equals("")) {
                        getUserHttp(authToken);
                    }
                    startActivity(new Intent(StartActivity.this, MainActivity.class));
                }
                finish();

            }
        }, 1000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }

    /**
     * 获取用户信息
     *
     * @param authToken
     */
    private void getUserHttp(String authToken) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/getProfile.ac");
        requestParams.addHeader(App.AUTH_TOKEN, authToken);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                App.user = gson.fromJson(result, new TypeToken<User>() {
                }.getType());
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }
}
