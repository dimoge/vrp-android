package com.shiyou.vrp.activity;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.asha.vrlib.MDVRLibrary;
import com.shiyou.vrp.App;
import com.shiyou.vrp.entity.Video;
import com.shiyou.vrp.widget.PlayerCommentFragment;
import com.shiyou.vrp.widget.PlayerDetailFragment;
import com.shiyou.vrp.utils.Utils;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import com.shiyou.vrp.R;

import java.io.IOException;

import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

public class PlayerActivity extends FragmentActivity implements IMediaPlayer.OnPreparedListener, IMediaPlayer.OnInfoListener, IMediaPlayer.OnBufferingUpdateListener {

    private static final int STATUS_IDLE = 0;
    private static final int STATUS_PREPARING = 1;
    private static final int STATUS_PREPARED = 2;
    private static final int STATUS_STARTED = 3;
    private static final int STATUS_PAUSED = 4;
    private static final int STATUS_STOPPED = 5;
    private int mStatus = STATUS_IDLE;
    private long lastPlayPostion = 0;           //上次播放的进度
    private long lastClickPosition = 0;         //上次唤醒工具条的时间

    private ToggleButton playpauseBtn;          // 启动、暂停按钮
    protected SeekBar time_Seekbar;             // 播放进度条
    protected TextView time_TextView;           // 时间长度
    private TextView time_duration_TextView;    // 时间总长度
    private String videoTimeString = null;      // 时间长度文本
    protected ToggleButton gyroBtn;             // 陀螺仪控制按钮
    protected ToggleButton screenBtn;           // 单双屏
    private ImageButton screenFullBtn;               //全屏切换按钮
    private ImageButton playerBackImgbtn;       // 返回按钮
    private ImageView imgBuffer;                // 缓冲动画
    private RelativeLayout rlToolbar;           //工具条布局
    private RelativeLayout rlPlayer;            //都包起来了,yes

    private PlayerCommentFragment commentFragment;           //评论的fragment
    private PlayerDetailFragment detailFragment;

    private MDVRLibrary mVRLibrary;
    private IMediaPlayer iMediaPlayer;

    private boolean current_sreen_is_portrait = true;//当前屏幕是竖屏吗?当然啦
    private String Path = "http://cache.utovr.com/201508270528174780.m3u8"; //setSource UVMediaType.UVMEDIA_TYPE_M3U8

    private Video video;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        /** 获取传递来的值 Video实体 **/
        video = (Video) getIntent().getSerializableExtra("VIDEO");
        Path = video.getVideoUrl();
        initView();
        setBufferVisibility(true);

        addWatchCount();//视频观看数量自增

        //初始化VRLibrary
        mVRLibrary = createVRLibrary();

        //初始化IMediaPlayer
        iMediaPlayer = new IjkMediaPlayer();
        mStatus = STATUS_IDLE;//开始
        if (iMediaPlayer instanceof IjkMediaPlayer) {
            IjkMediaPlayer player = (IjkMediaPlayer) iMediaPlayer;
            player.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "mediacodec", 1);
            player.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "mediacodec-auto-rotate", 1);
            player.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "overlay-format", IjkMediaPlayer.SDL_FCC_RV32);
            player.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "framedrop", 60);
            player.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "max-fps", 0);
            player.setOption(IjkMediaPlayer.OPT_CATEGORY_CODEC, "skip_loop_filter", 48);
        }
        iMediaPlayer.setOnPreparedListener(this);
        iMediaPlayer.setOnBufferingUpdateListener(this);

        try {
            iMediaPlayer.setDataSource(Path);
            iMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mVRLibrary.handleTouchEvent(event) || super.onTouchEvent(event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mVRLibrary.onResume(this);
        iMediaPlayer.start();//开始
        mStatus = STATUS_STARTED;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mVRLibrary.onPause(this);
        if (iMediaPlayer.isPlaying() && mStatus == STATUS_STARTED) {
            iMediaPlayer.pause();
            mStatus = STATUS_PAUSED;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mVRLibrary.onDestroy();
        iMediaPlayer.stop();
        if (iMediaPlayer != null) {
            iMediaPlayer.setSurface(null);
            iMediaPlayer.release();
        }
        iMediaPlayer = null;
        mStatus = STATUS_STOPPED;
    }

    /**
     * 初始化VRLibrary
     * 默认为 手动交互, 全屏播放 模式
     *
     * @return MDVRLibrary
     */
    private MDVRLibrary createVRLibrary() {
        return MDVRLibrary.with(PlayerActivity.this).displayMode(MDVRLibrary.DISPLAY_MODE_NORMAL)//普通播放模式
                .interactiveMode(MDVRLibrary.INTERACTIVE_MODE_TOUCH)//手动交互模式
                .video(new MDVRLibrary.IOnSurfaceReadyCallback() {//获取surface
                    @Override
                    public void onSurfaceReady(Surface surface) {
                        iMediaPlayer.setSurface(surface);
                    }
                })
                .ifNotSupport(new MDVRLibrary.INotSupportCallback() {//不支持
                    @Override
                    public void onNotSupport(int i) {

                    }
                })
                .pinchEnabled(true)
                .gesture(new MDVRLibrary.IGestureListener() {
                    @Override
                    public void onClick(MotionEvent motionEvent) {
                        lastClickPosition = iMediaPlayer.getCurrentPosition();
                        if (rlToolbar.getVisibility() != View.VISIBLE) {
                            rlToolbar.setVisibility(View.VISIBLE);
                        }
                    }
                }).build(R.id.surface_view);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //切换到横屏, 播放器全屏
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            rlPlayer.setLayoutParams(params);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            //切换到竖屏, 播放器240dp
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Utils.dip2px(PlayerActivity.this, 200));
            rlPlayer.setLayoutParams(params);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

    }

    @Override
    public void onBackPressed() {
        if (current_sreen_is_portrait) {
            super.onBackPressed();
        } else {
            //当前为横屏 , 切换到竖屏
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            current_sreen_is_portrait = true;
        }
    }

    private void initView() {
        // 工具栏上的按钮
        playerBackImgbtn = (ImageButton) findViewById(R.id.video_toolbar_back_imgbtn);
        rlToolbar = (RelativeLayout) findViewById(R.id.video_rlToolbar);//工具栏总布局
        gyroBtn = (ToggleButton) findViewById(R.id.video_toolbar_btn_gyro);// 陀螺仪
        screenBtn = (ToggleButton) findViewById(R.id.video_toolbar_btn_screen);// 单双屏
        playpauseBtn = (ToggleButton) findViewById(R.id.video_toolbar_btn_playpause);// 播放/暂停
        time_Seekbar = (SeekBar) findViewById(R.id.video_toolbar_time_seekbar);// 进度
        screenFullBtn = (ImageButton) findViewById(R.id.video_toolbar_btn_full);//全屏切换按钮
        rlPlayer = (RelativeLayout) findViewById(R.id.video_player_lyt);//包裹播放器和工具栏

        //初始化评论和视频详细的fragment
        commentFragment = new PlayerCommentFragment(video);
        detailFragment = new PlayerDetailFragment(video);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.player_comment_fragment, commentFragment);
        transaction.add(R.id.player_detail_fragment, detailFragment);
        transaction.commit();


        /** 横竖屏切换事件**/
        screenFullBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (current_sreen_is_portrait) {
                    //当前为竖屏 , 切换到横屏
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                    current_sreen_is_portrait = false;
                } else {
                    //当前为横屏 , 切换到竖屏
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                    current_sreen_is_portrait = true;
                }
            }
        });
        // 陀螺仪按钮事件
        gyroBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mVRLibrary != null) {
                    if (gyroBtn.isChecked()) {
                        mVRLibrary.switchInteractiveMode(PlayerActivity.this);
                    } else {
                        mVRLibrary.switchInteractiveMode(PlayerActivity.this);
                        mVRLibrary.switchInteractiveMode(PlayerActivity.this);
                    }

                }
            }
        });
        // 单双屏按钮事件
        screenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mVRLibrary != null) {
                    mVRLibrary.switchDisplayMode(PlayerActivity.this);
                }
            }
        });
        // 播放/暂停按钮事件
        playpauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((ToggleButton) v).isChecked()) {
                    iMediaPlayer.pause();
                    mStatus = STATUS_PAUSED;

                } else {
                    iMediaPlayer.start();
                    mStatus = STATUS_STARTED;
                }
            }
        });
        // 进度条事件
        time_Seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                long seekToWhere = seekBar.getProgress() * iMediaPlayer.getDuration() / 100;
                iMediaPlayer.seekTo(seekToWhere);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
            }
        });

        playerBackImgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        imgBuffer = (ImageView) findViewById(R.id.video_imgBuffer);
        time_TextView = (TextView) findViewById(R.id.video_toolbar_time_tv);// 时间
        time_duration_TextView = (TextView) findViewById(R.id.video_toolbar_time_duration_tv);//总时间
    }

    /**
     * 视频自增方法
     */
    private void addWatchCount() {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "video/addWatchCount.ac");
        requestParams.addBodyParameter("id", video.getId());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    //缓冲动画控制 显示/隐藏
    private void setBufferVisibility(boolean Visible) {
        if (Visible) {
            imgBuffer.setVisibility(View.VISIBLE);
            Utils.startImageAnim(imgBuffer, R.anim.play_buffer_anim);
        } else {
            Utils.stopImageAnim(imgBuffer);
            imgBuffer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPrepared(IMediaPlayer mp) {
        mStatus = STATUS_PREPARED;
        setBufferVisibility(false);
        if (iMediaPlayer == null) return;
        if (mStatus == STATUS_PREPARED || mStatus == STATUS_PAUSED) {
            iMediaPlayer.start();
            mStatus = STATUS_STARTED;
        }
    }

    @Override
    public boolean onInfo(IMediaPlayer mp, int what, int extra) {
        return false;
    }

    @Override
    public void onBufferingUpdate(IMediaPlayer mp, int percent) {
        //设置进度条前进
        int progress = (int) (mp.getCurrentPosition() * 100 / mp.getDuration());
        time_Seekbar.setProgress(progress);
        //设置时间显示
        time_duration_TextView.setText(Utils.getShowTime(mp.getDuration()));
        time_TextView.setText(Utils.getShowTime(mp.getCurrentPosition()));
        //显示缓冲
        if (mp.getCurrentPosition() == lastPlayPostion) {
            setBufferVisibility(true);
        } else {
            setBufferVisibility(false);
        }
        lastPlayPostion = mp.getCurrentPosition();
        if (mp.getCurrentPosition() - lastClickPosition > 4000) {
            //距离上次点击屏幕已经有4秒,隐藏工具条
            if (rlToolbar.getVisibility() == View.VISIBLE) {
                rlToolbar.setVisibility(View.INVISIBLE);
            }
        }
    }
}


