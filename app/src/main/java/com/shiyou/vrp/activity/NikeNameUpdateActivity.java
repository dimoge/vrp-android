package com.shiyou.vrp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.event.NickNameEvent;
import com.shiyou.vrp.utils.SharedPreferencesUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * Created by dime on 16/5/24.
 */
@ContentView(R.layout.activity_nike_name_update)
public class NikeNameUpdateActivity extends Activity {

    private String nikeName;

    @ViewInject(R.id.nike_name_edt)
    private EditText nikeNameEdt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        nikeName = getIntent().getStringExtra("NIKE_NAME");
        nikeNameEdt.setText(nikeName);
    }

    @Event(value = R.id.nike_upate_back_tv)
    private void back(View view) {
        finish();
    }

    @Event(value = R.id.nike_done_tv)
    private void done(View view) throws JSONException {
        if (TextUtils.isEmpty(nikeNameEdt.getText())) {
            Toast.makeText(x.app(), "昵称不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        final String saveName = nikeNameEdt.getText().toString().trim();
        if (saveName.equals("")) {
            Toast.makeText(x.app(), "昵称不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/modifyProfile.ac");
        requestParams.addHeader("AUTH-TOKEN", SharedPreferencesUtils.getString(App.AUTH_TOKEN));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("nickName", saveName);
        requestParams.addBodyParameter("userInfo", jsonObject.toString());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                EventBus.getDefault().post(new NickNameEvent(saveName));
                finish();
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    @Event(value = R.id.nike_name_delete_imgbtn)
    private void delete(View view) {
        nikeNameEdt.clearComposingText();
    }
}
