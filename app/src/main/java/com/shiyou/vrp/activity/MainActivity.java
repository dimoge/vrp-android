package com.shiyou.vrp.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.pgyersdk.javabean.AppBean;
import com.pgyersdk.update.PgyUpdateManager;
import com.pgyersdk.update.UpdateManagerListener;
import com.shiyou.vrp.R;
import com.shiyou.vrp.widget.GreatFragment;
import com.shiyou.vrp.widget.LocalFragment;
import com.shiyou.vrp.widget.MeFragment;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ContentView(R.layout.activity_main)
public class MainActivity extends FragmentActivity implements View.OnClickListener {

    private FragmentManager fragmentManager;
    private RelativeLayout tabGreatLyt, tabLocalLyt, tabMeLyt;

    @ViewInject(R.id.main_tab_great_img)
    ImageView mainTabGreatImg;
    @ViewInject(R.id.main_tab_local_img)
    ImageView mainTabLocalImg;
    @ViewInject(R.id.main_tab_me_img)
    ImageView mainTabMeImg;

    private static Fragment greatFragment = null;
    private static Fragment localFragment = null;
    private static Fragment meFragment = null;

    private List<Fragment> fragmentList = new ArrayList<>();
    private List<ImageView> tabImgList = new ArrayList<>();
    private List<Integer> tabImgResList = new ArrayList<>(Arrays.asList(R.mipmap.main_tab_home, R.mipmap.main_tab_local, R.mipmap.main_tab_me));
    private List<Integer> tabImgResPressList = new ArrayList<>(Arrays.asList(R.mipmap.main_tab_home_press, R.mipmap.main_tab_local_press, R.mipmap.main_tab_me_press));
    private int currentFragment = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        PgyUpdateManager.register(MainActivity.this,
                new UpdateManagerListener() {

                    @Override
                    public void onUpdateAvailable(final String result) {

                        // 将新版本信息封装到AppBean中
                        final AppBean appBean = getAppBeanFromString(result);
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("更新")
                                .setMessage("")
                                .setNegativeButton(
                                        "确定",
                                        new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(
                                                    DialogInterface dialog,
                                                    int which) {
                                                startDownloadTask(
                                                        MainActivity.this,
                                                        appBean.getDownloadURL());
                                            }
                                        }).show();
                    }

                    @Override
                    public void onNoUpdateAvailable() {
                    }
                });


        tabGreatLyt = (RelativeLayout) findViewById(R.id.main_tab_great_rlyt);
        tabLocalLyt = (RelativeLayout) findViewById(R.id.main_tab_local_rlyt);
        tabMeLyt = (RelativeLayout) findViewById(R.id.main_tab_me_rlyt);

        tabGreatLyt.setOnClickListener(this);
        tabLocalLyt.setOnClickListener(this);
        tabMeLyt.setOnClickListener(this);
        fragmentManager = getSupportFragmentManager();

        greatFragment = new GreatFragment();
        localFragment = new LocalFragment();
        meFragment = new MeFragment();

        fragmentList.add(greatFragment);
        fragmentList.add(localFragment);
        fragmentList.add(meFragment);

        tabImgList.add(mainTabGreatImg);
        tabImgList.add(mainTabLocalImg);
        tabImgList.add(mainTabMeImg);

        //初始化界面, 显示"精选"界面
        FragmentTransaction transaction = fragmentManager.beginTransaction();//创建事务
        transaction.add(R.id.main_fragment, greatFragment);
        transaction.commit();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.main_tab_great_rlyt:
                tabSelect(0);
                break;
            case R.id.main_tab_local_rlyt:
                tabSelect(1);
                break;
            case R.id.main_tab_me_rlyt:
                tabSelect(2);
                break;
            default:
                return;
        }
    }

    private void tabSelect(int position) {
        //切换tab背景
        for (int i = 0; i < tabImgList.size(); i++) {
            if (i != position) {
                tabImgList.get(i).setImageResource(tabImgResList.get(i));
            } else {
                tabImgList.get(i).setImageResource(tabImgResPressList.get(i));
            }
        }


        //切换fragment
        if (currentFragment != position) {
            if (fragmentManager == null) {
                fragmentManager = getSupportFragmentManager();
            }
            FragmentTransaction transaction = fragmentManager.beginTransaction();//创建事务
            transaction.hide(fragmentList.get(currentFragment));
            if (fragmentList.get(position).isAdded()) {
                transaction.hide(fragmentList.get(currentFragment)).show(fragmentList.get(position));
            } else {
                transaction.hide(fragmentList.get(currentFragment)).add(R.id.main_fragment, fragmentList.get(position));
            }
            transaction.commit();
            currentFragment = position;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
