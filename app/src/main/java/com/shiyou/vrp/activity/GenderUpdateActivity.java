package com.shiyou.vrp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.event.GenderEvent;
import com.shiyou.vrp.utils.SharedPreferencesUtils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * Created by dime on 16/5/25.
 */
@ContentView(R.layout.activity_gender_update)
public class GenderUpdateActivity extends Activity {

    private String gender;

    @ViewInject(R.id.gender_male_img)
    private ImageView maleImg;
    @ViewInject(R.id.gender_female_img)
    private ImageView femaleImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        gender = getIntent().getStringExtra("GENDER");
        if (gender.equals("女")) {
            maleImg.setVisibility(View.INVISIBLE);
            femaleImg.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 取消按钮
     *
     * @param view
     */
    @Event(value = R.id.gender_back_tv)
    private void back(View view) {
        onBackPressed();
    }

    @Event(value = R.id.gender_male_lyt)
    private void seleteMale(View view) {
        if (gender.equals("男")) return;
        gender = "男";
        genderUpdateHttp(gender);
    }

    @Event(value = R.id.gender_female_lyt)
    private void selectFemale(View view) {
        if (gender.equals("女")) return;
        gender = "女";
        genderUpdateHttp(gender);
    }

    private void genderUpdateHttp(final String gender) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/modifyProfile.ac");
        requestParams.addHeader(App.AUTH_TOKEN, SharedPreferencesUtils.getString(App.AUTH_TOKEN));
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("gender", gender);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestParams.addBodyParameter("userInfo", jsonObject.toString());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                finish();
                GenderEvent genderEvent = new GenderEvent(gender);
                EventBus.getDefault().post(genderEvent);//发布事件>>>性别修改成功
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }
}
