package com.shiyou.vrp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.adapter.LocalRecentAdapter;
import com.shiyou.vrp.entity.Local;

import org.xutils.DbManager;
import org.xutils.ex.DbException;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dime on 16/5/25.
 */
@ContentView(R.layout.activity_local_list)
public class LocalListActivity extends Activity {

    @ViewInject(R.id.local_list_recylerview)
    private RecyclerView recyclerView;

    private List<Local> localList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        try {
            String parent = getIntent().getStringExtra("PARENT");

            DbManager db = x.getDb(App.getDaoConfig());
            localList = db.selector(Local.class).where("parent", "=", parent).findAll();
        } catch (DbException e) {
            e.printStackTrace();
        }finally {
            if(localList == null){
                localList = new ArrayList<>();
            }
        }

        LocalRecentAdapter localRecentAdapter = new LocalRecentAdapter(this, localList);
        recyclerView.setAdapter(localRecentAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(LocalListActivity.this));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * 返回按钮
     * @param view
     */
    @Event(value = R.id.local_list_back_imgbtn)
    private void go2back(View view){
        onBackPressed();
    }
}
