package com.shiyou.vrp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.adapter.SearchRecylerViewAdapter;
import com.shiyou.vrp.entity.Video;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dime on 16/5/9.
 */
@ContentView(R.layout.activity_search)
public class SearchActivity extends AppCompatActivity {

    @ViewInject(R.id.search_input_edt)
    EditText inputEdt;
    @ViewInject(R.id.search_recylerview)
    RecyclerView recyclerView;

    private String key;
    private List<Video> videoList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);
    }

    @Event(value = R.id.search_back_imgbtn)
    private void back(View view) {
        finish();
    }

    @Event(value = R.id.search_imgbtn)
    private void search(View view) {
        key = inputEdt.getText().toString().trim();
        if (!key.equals("")) {
            searchByKey(key);
        } else {
            Toast.makeText(SearchActivity.this, "请输入搜索关键字", Toast.LENGTH_LONG).show();
        }

    }

    private void searchByKey(String key) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "video/search.ac");
        requestParams.addBodyParameter("keyword", key);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                videoList = gson.fromJson(result, new TypeToken<List<Video>>() {}.getType());
                if (videoList.size() == 0) {
                    Toast.makeText(SearchActivity.this, "未找到相符的视频^_^", Toast.LENGTH_LONG).show();
                    return;
                }

                recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
                SearchRecylerViewAdapter searchRecylerViewAdapter = new SearchRecylerViewAdapter(SearchActivity.this, videoList);
                recyclerView.setAdapter(searchRecylerViewAdapter);

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Toast.makeText(SearchActivity.this, "请检查网络连接...........", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFinished() {

            }
        });
    }
}
