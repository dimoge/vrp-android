package com.shiyou.vrp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.shiyou.vrp.App;
import com.shiyou.vrp.R;

import org.w3c.dom.Text;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

/**
 * Created by dime on 16/5/26.
 */
@ContentView(R.layout.activity_feed_back)
public class FeedBackActivity extends Activity {

    @ViewInject(R.id.feed_back_imgbtn)
    private ImageButton back;
    @ViewInject(R.id.feed_done_tv)
    private TextView doneTv;
    @ViewInject(R.id.feed_content_edit)
    private EditText contentEdit;
    @ViewInject(R.id.feed_who_edit)
    private EditText whoEdit;

    private String content;
    private String who;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);
    }

    @Event(value = R.id.feed_done_tv)
    private void feedDone(View view) {
        if (TextUtils.isEmpty(contentEdit.getText())) {
            Toast.makeText(FeedBackActivity.this, "反馈意见不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        content = contentEdit.getText().toString();
        if (TextUtils.isEmpty(whoEdit.getText())) {
            Toast.makeText(FeedBackActivity.this, "联系方式不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        who = whoEdit.getText().toString();
        feedHttp(content, who);
    }

    @Event(value = R.id.feed_back_imgbtn)
    private void back(View view) {
        finish();
    }

    private void feedHttp(String content, String who) {

        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/addFeedback.ac");
        requestParams.addBodyParameter("content", content);
        requestParams.addBodyParameter("contact", who);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(FeedBackActivity.this, "反馈意见提交成功,谢谢您的支持!!!", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });

    }
}
