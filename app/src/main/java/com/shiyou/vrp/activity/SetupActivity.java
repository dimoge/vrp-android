package com.shiyou.vrp.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.utils.SharedPreferencesUtils;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.File;

/**
 * Created by dime on 2016/5/23 0023.
 */
@ContentView(R.layout.activity_setup)
public class SetupActivity extends Activity {

    @ViewInject(R.id.setup_wifi_switch)
    private Switch onlyWifiSwitch;
    @ViewInject(R.id.setup_cache_size_tv)
    private TextView cacheSizeTv;
    @ViewInject(R.id.setup_unlogin_lyt)
    private LinearLayout unLoginLyt;

    private double cache_size = 0;//cache/目录下的所有文件大小, 不包括video文件夹
    private boolean ONLY_WIFI = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        File file = new File(SharedPreferencesUtils.getString(App.CACHE_PATH));
        cache_size = getDirSize(file);

        cacheSizeTv.setText(new java.text.DecimalFormat("#.00").format(cache_size) + "M");
        ONLY_WIFI = SharedPreferencesUtils.getBoolean(App.ONLY_WIFI);
        onlyWifiSwitch.setChecked(ONLY_WIFI);
        if (SharedPreferencesUtils.getString(App.AUTH_TOKEN).equals("")) {
            unLoginLyt.setVisibility(View.INVISIBLE);
        }

        onlyWifiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ONLY_WIFI = b;
                SharedPreferencesUtils.set(App.ONLY_WIFI, b);
                onlyWifiSwitch.setChecked(b);
            }
        });
    }

    /**
     * 清理缓存, 并不清理视频文件
     *
     * @param view
     */
    @Event(value = R.id.setup_clear_cache_lyt)
    private void doClearCache(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(SetupActivity.this);
        builder.setPositiveButton("清理", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                x.image().clearCacheFiles();
                cacheSizeTv.setText("0M");
                dialogInterface.dismiss();
            }
        }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).setMessage("清理后,下次加载将消耗一定的流量").setTitle("提示");
        builder.create().show();

    }

    /**
     * 检查更新版本
     *
     * @param view
     */
    @Event(value = R.id.setup_check_version_lyt)
    private void checkVersion(View view) {
        Toast.makeText(SetupActivity.this, "当前是最新版本", Toast.LENGTH_SHORT).show();
    }

    /**
     * 跳转到反馈界面
     *
     * @param view
     */
    @Event(value = R.id.setup_feedback_lyt)
    private void go2FeedBackActivity(View view) {
        startActivity(new Intent(SetupActivity.this, FeedBackActivity.class));
    }

    /**
     * 退出登录
     *
     * @param view
     */
    @Event(value = R.id.setup_unlogin_lyt)
    private void loginOut(View view) {
        SharedPreferencesUtils.set(App.AUTH_TOKEN, "");
        startActivity(new Intent(SetupActivity.this, LoginActivity.class));
        finish();
    }

    /**
     * 跳转到关于我们界面
     *
     * @param view
     */
    @Event(value = R.id.setup_about_lyt)
    private void go2AbortActivity(View view) {
        startActivity(new Intent(SetupActivity.this, AbortActivity.class));
    }

    /**
     * 返回
     *
     * @param view
     */
    @Event(value = R.id.setup_back_imgbtn)
    private void goBack(View view) {
        finish();
    }

    /**
     * 计算一个目录的size
     *
     * @param dir
     * @return
     */
    private double getDirSize(File dir) {
        //判断文件是否存在
        if (dir.exists()) {
            //如果是目录则递归计算其内容的总大小
            if (dir.isDirectory()) {
                File[] children = dir.listFiles();
                double size = 0;
                for (File f : children)
                    size += getDirSize(f);
                return size;
            } else {//如果是文件则直接返回其大小,以“兆”为单位
                double size = (double) dir.length() / 1024 / 1024;
                return size;
            }
        } else {
            return 0.0;
        }
    }
}
