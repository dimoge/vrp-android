package com.shiyou.vrp.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.entity.VrpError;
import com.shiyou.vrp.utils.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.ex.HttpException;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.net.ConnectException;

/**
 * Created by dime on 2016/5/23 0023.
 */
@ContentView(R.layout.activity_login)
public class LoginActivity extends Activity {

    @ViewInject(R.id.login_no_tv)
    private TextView noLoginTv;
    @ViewInject(R.id.login_go2register_tv)
    private TextView registerTv;
    @ViewInject(R.id.login_btn)
    private Button loginBtn;

    @ViewInject(R.id.login_emial_edt)
    private EditText emailEdt;
    @ViewInject(R.id.login_pwd_edt)
    private EditText pwdEdit;

    private String email = "";
    private String password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        registerTv.setText(Html.fromHtml("还没有账号?赶紧" + "<u>" + "注册一个" + "</u>" + "吧!"));
    }

    @Event(value = R.id.login_go2register_tv)
    private void go2register(View view) {
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }

    @Event(value = R.id.login_btn)
    private void login(View view) {
        if (TextUtils.isEmpty(emailEdt.getText())) {
            Toast.makeText(LoginActivity.this, "邮箱不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        email = emailEdt.getText().toString().trim();
        if (TextUtils.isEmpty(pwdEdit.getText())) {
            Toast.makeText(LoginActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        password = pwdEdit.getText().toString().trim();
        loginHttp();
    }

    @Event(value = R.id.login_no_tv)
    private void noLogin(View view) {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    /**
     * 登陆网络请求
     */
    private void loginHttp() {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/login.ac");
        requestParams.addBodyParameter("email", email);
        requestParams.addBodyParameter("password", password);
        x.http().post(requestParams, new Callback.CommonCallback<JSONObject>() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    SharedPreferencesUtils.set(App.AUTH_TOKEN, result.getString("authToken"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Toast.makeText(LoginActivity.this, "登陆成功!!!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                if (ex instanceof HttpException) {
                    VrpError vrpError = new Gson().fromJson(((HttpException) ex).getResult(), new TypeToken<VrpError>() {
                    }.getType());
                    Toast.makeText(LoginActivity.this, "登录失败:" + vrpError.getMessage(), Toast.LENGTH_LONG).show();
                }
                if (ex instanceof ConnectException) {
                    Toast.makeText(LoginActivity.this, "登录失败:请检查网络环境", Toast.LENGTH_LONG).show();
                }

                pwdEdit.setText("");
            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }
}
