package com.shiyou.vrp.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import com.shiyou.vrp.R;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.x;

/**
 * Created by dime on 16/5/26.
 */
@ContentView(R.layout.activity_abort)
public class AbortActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);
    }

    @Event(value = R.id.about_back_imgbtn)
    private void back(View view) {
        onBackPressed();
    }
}
