package com.shiyou.vrp.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.entity.VrpError;

import org.xutils.common.Callback;
import org.xutils.ex.HttpException;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.net.ConnectException;

/**
 * Created by dime on 2016/5/23 0023.
 */
@ContentView(R.layout.activity_register)
public class RegisterActivity extends Activity {

    @ViewInject(R.id.register_email_edt)
    EditText emailEdt;
    @ViewInject(R.id.register_pwd_edt)
    EditText pwdEdt;
    @ViewInject(R.id.register_btn)
    Button registerBtn;

    private String email;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        emailEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (TextUtils.isEmpty(emailEdt.getText())) {
                        Toast.makeText(RegisterActivity.this, "邮箱不能为空", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    email = emailEdt.getText().toString().trim();
                    String regex = "\\w+(\\.\\w)*@\\w+(\\.\\w{2,3}){1,3}";
                    if (!email.matches(regex)) {
                        Toast.makeText(RegisterActivity.this, "邮箱格式错误", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    validEmail(email);
                }
            }
        });

        pwdEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (TextUtils.isEmpty(emailEdt.getText())) {
                    Toast.makeText(RegisterActivity.this, "邮箱不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                password = pwdEdt.getText().toString().trim();
                String regex = "\\w+(\\.\\w)*@\\w+(\\.\\w{2,3}){1,3}";
                if (!email.matches(regex)) {
                    Toast.makeText(RegisterActivity.this, "邮箱格式错误", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!hasFocus) {
                    if (TextUtils.isEmpty(emailEdt.getText())) {
                        Toast.makeText(RegisterActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    password = emailEdt.getText().toString().trim();
                    if (password.length() < 6) {
                        Toast.makeText(RegisterActivity.this, "密码至少六位", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
        });
    }

    @Event(value = R.id.register_btn)
    private void register(View view) {
        if (TextUtils.isEmpty(emailEdt.getText())) {
            Toast.makeText(RegisterActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        password = pwdEdt.getText().toString().trim();
        if (password.length() < 6) {
            Toast.makeText(RegisterActivity.this, "密码至少六位", Toast.LENGTH_SHORT).show();
            return;
        }
        registerHttp();
    }

    private void validEmail(String input) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/valid.ac");
        requestParams.addBodyParameter("email", input);
        x.http().post(requestParams, new Callback.CommonCallback<Boolean>() {
            @Override
            public void onSuccess(Boolean result) {
                if (!result) {
                    Toast.makeText(RegisterActivity.this, "邮箱已经被注册", Toast.LENGTH_SHORT).show();
                    emailEdt.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                } else {
                    emailEdt.setPaintFlags(0);
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Toast.makeText(x.app(), "邮箱验证失败", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    private void registerHttp() {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/register.ac");
        requestParams.addBodyParameter("email", email);
        requestParams.addBodyParameter("password", password);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(RegisterActivity.this, "注册成功!!!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                finish();
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                if (ex instanceof HttpException) {
                    VrpError vrpError = new Gson().fromJson(((HttpException) ex).getResult(), new TypeToken<VrpError>() {
                    }.getType());
                    Toast.makeText(RegisterActivity.this, "注册失败:" + vrpError.getMessage(), Toast.LENGTH_LONG).show();
                }
                if (ex instanceof ConnectException) {
                    Toast.makeText(RegisterActivity.this, "注册失败:请检查网络环境", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

}
