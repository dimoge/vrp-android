package com.shiyou.vrp.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.entity.Catelogue;
import com.shiyou.vrp.entity.Local;

import org.xutils.DbManager;
import org.xutils.ex.DbException;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dime on 2016/5/19 0019.
 */
@ContentView(R.layout.activity_file)
public class FileActivity extends Activity {

    @ViewInject(R.id.file_recylerview)
    RecyclerView recyclerView;
    @ViewInject(R.id.file_current_path_tv)
    private TextView currentPaht;

    LinearLayoutManager linearLayoutManager;
    FileAdapter fileAdapter;

    File rootFile;

    DbManager db;

    private List<File> data = new ArrayList<>();//这里是数据源
    private List<File> select = new ArrayList<>();//选中的mp4

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        linearLayoutManager = new LinearLayoutManager(this);
        fileAdapter = new FileAdapter();
        rootFile = Environment.getExternalStorageDirectory();
        currentPaht.setText(rootFile.getAbsolutePath());

        data = filterFile(rootFile);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(fileAdapter);
    }

    private class FileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 0) {
                View view = LayoutInflater.from(FileActivity.this).inflate(R.layout.item_file_directory, parent, false);
                DirectoryHolder holder = new DirectoryHolder(view);
                return holder;
            } else {
                View view = LayoutInflater.from(FileActivity.this).inflate(R.layout.item_file_mp4, parent, false);
                FileHolder holder = new FileHolder(view);
                return holder;
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            if (holder instanceof DirectoryHolder) {
                ((DirectoryHolder) holder).nameTv.setText(data.get(position).getName());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        rootFile = data.get(position);
                        updateData();
                    }
                });
            }
            if (holder instanceof FileHolder) {
                ((FileHolder) holder).nameTv.setText(data.get(position).getName());
                ((FileHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean tag = (boolean) v.getTag();
                        if (tag == false) {
                            v.setBackgroundColor(Color.parseColor("#D6D6D6"));//#fffafafa
                            v.setTag(true);
                            select.add(data.get(position));
                        } else {
                            v.setBackgroundColor(Color.parseColor("#efefef"));
                            v.setTag(false);
                            select.remove(data.get(position));
                        }

                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        @Override
        public int getItemViewType(int position) {
            return data.get(position).isDirectory() ? 0 : 1;
        }

        private void updateData() {
            for (File file : select) {
                recyclerView.getChildAt(data.indexOf(file)).setTag(false);
                recyclerView.getChildAt(data.indexOf(file)).setBackgroundColor(Color.parseColor("#fffafafa"));
            }
            data = filterFile(rootFile);
            fileAdapter.notifyDataSetChanged();
            currentPaht.setText(rootFile.getAbsolutePath());
            select.clear();
        }
    }

    private class FileHolder extends RecyclerView.ViewHolder {

        private TextView nameTv;

        public FileHolder(View itemView) {
            super(itemView);
            nameTv = (TextView) itemView.findViewById(R.id.file_mp4_name_tv);
            itemView.setTag(false);
        }
    }

    private class DirectoryHolder extends RecyclerView.ViewHolder {

        private TextView nameTv;

        public DirectoryHolder(View itemView) {
            super(itemView);
            nameTv = (TextView) itemView.findViewById(R.id.file_dir_name_tv);
        }
    }

    @Event(value = R.id.file_back)
    private void go2ParentFile(View view) {
        if (rootFile.getAbsolutePath().split("/").length == 2) {
            onBackPressed();
        } else {
            rootFile = rootFile.getParentFile();
            fileAdapter.updateData();
        }
    }

    @Event(value = R.id.file_select_tv)
    private void select(View view) {
        if (select.size() > 0) {
            db = x.getDb(App.getDaoConfig());
            try {
                Catelogue catelogue = new Catelogue();
                catelogue.setName(select.get(0).getParentFile().getAbsolutePath());
                db.save(catelogue);//保存下目录, 数据库唯一约束, 不怕重复
                for (File file : select) {
                    Local save = new Local();
                    save.setPath(file.getAbsolutePath());//unique
                    save.setParent(catelogue.getName());
                    save.setTitle(file.getName());
                    save.setDate(new Date(0));
                    db.save(save);//数据库唯一约束, 不怕重复
                }
            } catch (DbException e) {
                e.printStackTrace();
            }
        }
        Intent intent = new Intent(FileActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * 剔除非mp4文件,形成列表
     *
     * @return
     */
    private List<File> filterFile(File file) {
        if (!file.isDirectory()) return null;
        List<File> wellList = new ArrayList<>();
        for (File target : file.listFiles()) {
            if (target.isDirectory()) {
                wellList.add(target);
            } else if (target.getName().endsWith(".mp4")) {
                wellList.add(target);
            }
        }
        return wellList;
    }
}
