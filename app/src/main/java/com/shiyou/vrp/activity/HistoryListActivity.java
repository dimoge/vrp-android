package com.shiyou.vrp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.adapter.HistoryListAdapter;
import com.shiyou.vrp.adapter.SearchRecylerViewAdapter;
import com.shiyou.vrp.entity.History;
import com.shiyou.vrp.entity.Video;
import com.shiyou.vrp.utils.SharedPreferencesUtils;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dime on 2016/5/26 0026.
 */
@ContentView(R.layout.activity_history_list)
public class HistoryListActivity extends Activity {

    @ViewInject(R.id.listView)
    private SwipeMenuListView listView;

    @ViewInject(R.id.history_login_tv)
    private TextView loginTv;
    @ViewInject(R.id.history_nologin_lyt)
    private LinearLayout noLoginLyt;
    @ViewInject(R.id.history_nodata_lyt)
    private LinearLayout noDataLyt;

    private String authToken;//token
    private List<History> historyList;
    private List<Video> videoList = new ArrayList<>();

    SwipeMenuCreator creator;
    HistoryListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem item = new SwipeMenuItem(x.app());
                item.setWidth(300);
                item.setTitle("删除");
                item.setTitleSize(15);
                item.setTitleColor(R.color.whiteColor);
                item.setBackground(R.color.mainColor);

                menu.addMenuItem(item);
            }
        };

        authToken = SharedPreferencesUtils.getString(App.AUTH_TOKEN);
        if (authToken.equals("")) {//用户没登陆
            listView.setVisibility(View.INVISIBLE);
            noLoginLyt.setVisibility(View.VISIBLE);
            return;
        }
        getHistoryHttp(authToken);
    }

    /**
     * 去登陆
     *
     * @param view
     */
    @Event(value = R.id.history_login_tv)
    private void go2LoginActivity(View view) {
        startActivity(new Intent(HistoryListActivity.this, LoginActivity.class));
        finish();
    }

    /**
     * 返回按钮
     *
     * @param view
     */
    @Event(value = R.id.history_back_imgbtn)
    private void back(View view) {
        onBackPressed();
    }

    /**
     * 获取观看历史记录网络请求
     *
     * @param token
     */
    private void getHistoryHttp(String token) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "history/getHistory.ac");
        requestParams.addHeader(App.AUTH_TOKEN, token);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                historyList = gson.fromJson(result, new TypeToken<List<History>>() {
                }.getType());
                for (History history : historyList) {
                    videoList.add(history.getVideo());
                }
                if (videoList.size() == 0) {
                    noDataLyt.setVisibility(View.VISIBLE);
                    listView.setVisibility(View.INVISIBLE);
                    return;
                }
                adapter = new HistoryListAdapter(x.app(), videoList);
                listView.setMenuCreator(creator);
                listView.setAdapter(adapter);
                //滑动删除item
                listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                        //index是MenuItem的编号, position是listview的位置
                        deleteCollectt(videoList.get(position).getId(), position);
                        return false;
                    }
                });
                //点击播放
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Video video = videoList.get(i);
                        Intent intent = new Intent(HistoryListActivity.this, PlayerActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("VIDEO", video);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 删除用户收藏
     *
     * @param vid 视频id
     */
    private void deleteCollectt(String vid, final int position) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "history/remove.ac");
        requestParams.addHeader(App.AUTH_TOKEN, SharedPreferencesUtils.getString(App.AUTH_TOKEN));
        requestParams.addBodyParameter("vid", vid);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                //删除成功后, 更新数据和界面
                videoList.remove(position);
                adapter.updateData(videoList);
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                Toast.makeText(x.app(), "删除失败", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }
}
