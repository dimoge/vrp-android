package com.shiyou.vrp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.shiyou.vrp.R;
import com.shiyou.vrp.download.DownloadInfo;
import com.shiyou.vrp.download.DownloadManager;
import com.shiyou.vrp.download.DownloadState;
import com.shiyou.vrp.download.DownloadViewHolder;
import com.shiyou.vrp.entity.Local;
import com.shiyou.vrp.utils.Utils;

import org.xutils.common.Callback;
import org.xutils.ex.DbException;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.File;

/**
 * Created by dime on 16/5/16.
 */
@ContentView(R.layout.activity_download)
public class DownloadActivity extends Activity {

    @ViewInject(R.id.download_listview)
    private SwipeMenuListView listView;

    private DownloadManager downloadManager;
    private DownloadListAdapter downloadListAdapter;

    SwipeMenuCreator creator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);

        //创建侧滑菜单
        creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem del = new SwipeMenuItem(DownloadActivity.this);
                del.setWidth(300);
                del.setTitle("删除");
                del.setTitleSize(15);
                del.setTitleColor(R.color.whiteColor);
                del.setBackground(R.color.mainColor);

                menu.addMenuItem(del);
            }
        };

        downloadManager = DownloadManager.getInstance();//获取下载管理实例
        downloadListAdapter = new DownloadListAdapter();
        listView.setAdapter(downloadListAdapter);
        listView.setMenuCreator(creator);
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                DownloadInfo forDel = downloadManager.getDownloadInfo(position);
                if (forDel.getState().value() == DownloadState.STARTED.value()) {
                    Toast.makeText(DownloadActivity.this, "请先停止下载", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        downloadManager.removeDownload(forDel);
                        downloadListAdapter.notifyDataSetChanged();
                    } catch (DbException e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }
        });

    }

    @Event(value = R.id.download_back)
    private void downloadBack(View view) {
        onBackPressed();
    }

    private class DownloadListAdapter extends BaseAdapter {

        private Context mContext;
        private final LayoutInflater mInflater;

        private DownloadListAdapter() {
            mContext = getBaseContext();
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            if (downloadManager == null) return 0;
            return downloadManager.getDownloadListCount();
        }

        @Override
        public Object getItem(int i) {
            return downloadManager.getDownloadInfo(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            DownloadItemViewHolder holder = null;
            DownloadInfo downloadInfo = downloadManager.getDownloadInfo(i);
            if (view == null) {
                view = mInflater.inflate(R.layout.download_item, null);
                holder = new DownloadItemViewHolder(view, downloadInfo);
                view.setTag(holder);
                x.image().bind(holder.img, downloadInfo.getCoverUrl());
                holder.refresh();
            } else {
                holder = (DownloadItemViewHolder) view.getTag();
                holder.update(downloadInfo);
            }

            if (downloadInfo.getState().value() < DownloadState.FINISHED.value()) {
                try {
                    downloadManager.startDownload(
                            downloadInfo.getUrl(),
                            downloadInfo.getCoverUrl(),
                            downloadInfo.getLabel(),
                            downloadInfo.getFileSavePath(),
                            downloadInfo.isAutoResume(),
                            downloadInfo.isAutoRename(),
                            holder);
                } catch (DbException ex) {
                    Toast.makeText(x.app(), "添加下载失败", Toast.LENGTH_LONG).show();
                }
            }

            return view;
        }
    }

    public class DownloadItemViewHolder extends DownloadViewHolder {
        @ViewInject(R.id.download_item_img)
        private ImageView img;
        @ViewInject(R.id.download_label)
        TextView label;
        @ViewInject(R.id.download_state)
        TextView state;
        @ViewInject(R.id.download_pb)
        ProgressBar progressBar;
        @ViewInject(R.id.download_item_size)
        TextView sizeTv;

        public DownloadItemViewHolder(View view, DownloadInfo downloadInfo) {
            super(view, downloadInfo);
            refresh();
        }

        @Event(R.id.download_item_img)
        private void toggleEvent(View view) {
            DownloadState state = downloadInfo.getState();
            switch (state) {
                case WAITING:
                case STARTED:
                    downloadManager.stopDownload(downloadInfo);
                    break;
                case ERROR:
                case STOPPED:
                    try {
                        downloadManager.startDownload(
                                downloadInfo.getUrl(),
                                downloadInfo.getUrl(),
                                downloadInfo.getLabel(),
                                downloadInfo.getFileSavePath(),
                                downloadInfo.isAutoResume(),
                                downloadInfo.isAutoRename(),
                                this);
                    } catch (DbException ex) {
                        Toast.makeText(x.app(), "添加下载失败", Toast.LENGTH_LONG).show();
                    }
                    break;
                case FINISHED:
                    Local local = new Local();
                    local.setPath(downloadInfo.getFileSavePath());
                    local.setTitle(downloadInfo.getLabel());
                    Intent intent = new Intent(DownloadActivity.this, LocalPlayerActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("LOCAL", local);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    break;
                default:
                    break;
            }
        }

        @Event(R.id.download_remove_btn)
        private void removeEvent(View view) {
            try {
                downloadManager.removeDownload(downloadInfo);
                downloadListAdapter.notifyDataSetChanged();
            } catch (DbException e) {
                Toast.makeText(x.app(), "移除任务失败", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void update(DownloadInfo downloadInfo) {
            super.update(downloadInfo);
            refresh();
        }

        @Override
        public void onWaiting() {
            refresh();
        }

        @Override
        public void onStarted() {
            refresh();
        }

        @Override
        public void onLoading(long total, long current) {
            refresh();
        }

        @Override
        public void onSuccess(File result) {
            refresh();
        }

        @Override
        public void onError(Throwable ex, boolean isOnCallback) {
            refresh();
        }

        @Override
        public void onCancelled(Callback.CancelledException cex) {
            refresh();
        }

        public void refresh() {
            label.setText(downloadInfo.getLabel());
            state.setText(Utils.downloadState2String(downloadInfo.getState()));
            progressBar.setProgress(downloadInfo.getProgress());
            sizeTv.setText(Utils.longFormat(downloadInfo.getCurrent()) + "M" + "/" + Utils.longFormat(downloadInfo.getTotal()) + "M");
            DownloadState state = downloadInfo.getState();
            switch (state) {
                case WAITING:
                case STARTED:
                    break;
                case ERROR:
                case STOPPED:
                    break;
                case FINISHED:
                    break;
                default:
                    break;
            }
        }
    }

}
