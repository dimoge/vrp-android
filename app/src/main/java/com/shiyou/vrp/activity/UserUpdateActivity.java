package com.shiyou.vrp.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.entity.User;
import com.shiyou.vrp.entity.VrpError;
import com.shiyou.vrp.event.AvatarEvent;
import com.shiyou.vrp.event.GenderEvent;
import com.shiyou.vrp.event.NickNameEvent;
import com.shiyou.vrp.utils.SharedPreferencesUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.common.util.DensityUtil;
import org.xutils.ex.HttpException;
import org.xutils.http.RequestParams;
import org.xutils.image.ImageOptions;
import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;
import org.xutils.x;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.URI;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dime on 2016/5/23 0023.
 */
@ContentView(R.layout.activity_user_update)
public class UserUpdateActivity extends Activity {

    private final int ALBUM_ACTION = 0;
    private final int CAMERA_ACTION = 1;
    private final int CLIP_ALBUM_ACTION = 2;
    private final int CLIP_CAMERA_ACTION = 3;

    private User user;
    private String avatarUrl;

    @ViewInject(R.id.user_update_back_imgbtn)
    private ImageButton backBtn;
    @ViewInject(R.id.user_update_avatar_cimg)
    private CircleImageView headCimg;
    @ViewInject(R.id.user_update_nikename_tv)
    private TextView nikeNameTv;
    @ViewInject(R.id.user_update_sex_tv)
    private TextView sexTv;
    @ViewInject(R.id.user_update_nikename_lyt)
    private LinearLayout nikeNameLyt;
    @ViewInject(R.id.user_update_sex_lyt)
    private LinearLayout sexLyt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        x.view().inject(this);
        EventBus.getDefault().register(this);

        //接受传递的user参数
        user = (User) getIntent().getSerializableExtra("USER");
        //初始化界面
        loadAvatar(user.getAvatarUrl(), headCimg);
        nikeNameTv.setText(user.getNikeName());
        sexTv.setText(user.getGender());

    }

    @Subscribe
    public void onMessageEvent(NickNameEvent nickNameEvent) {
        user.setNikeName(nickNameEvent.getName());
        nikeNameTv.setText(nickNameEvent.getName());
    }

    @Subscribe
    public void onMessageEvent(GenderEvent genderEvent) {
        sexTv.setText(genderEvent.getGender());
        user.setGender(genderEvent.getGender());
    }

    /**
     * 跳转到昵称修改界面
     *
     * @param view
     */
    @Event(value = R.id.user_update_nikename_lyt)
    private void go2NikeNameUpdate(View view) {
        Intent intent = new Intent(UserUpdateActivity.this, NikeNameUpdateActivity.class);
        intent.putExtra("NIKE_NAME", user.getNikeName());
        startActivity(intent);
    }

    /**
     * 跳转到修改性别界面
     *
     * @param view
     */
    @Event(value = R.id.user_update_sex_lyt)
    private void go2SexUpdate(View view) {
        Intent intent = new Intent(UserUpdateActivity.this, GenderUpdateActivity.class);
        intent.putExtra("GENDER", user.getGender());
        startActivity(intent);
    }

    /**
     * 返回按钮
     *
     * @param view
     */
    @Event(value = R.id.user_update_back_imgbtn)
    private void back(View view) {
        onBackPressed();
    }

    /**
     * 打开相册, 更换头像
     *
     * @param view
     */
    @Event(value = R.id.user_update_avatar_cimg)
    private void openAlbum(View view) {
        Intent intent = new Intent("android.intent.action.PICK");
        intent.setType("image/*");
        startActivityForResult(intent, ALBUM_ACTION);
    }

    /**
     * 打开相机, 更换头像
     *
     * @param view
     */
    @Event(value = R.id.user_update_camera_imgbtn)
    private void openCamera(View view) {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, CAMERA_ACTION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case ALBUM_ACTION://相册返回
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    //跳转到裁剪
                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(uri, "image/*");
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("outputX", 100);
                    intent.putExtra("outputY", 100);
                    intent.putExtra("scale", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, CLIP_ALBUM_ACTION);
                }
                break;
            case CAMERA_ACTION://相机返回
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(uri, "image/*");
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("outputX", 100);
                    intent.putExtra("outputY", 100);
                    intent.putExtra("scale", true);
                    intent.putExtra("return-data", true);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    startActivityForResult(intent, CLIP_ALBUM_ACTION);
                }
                break;
            case CLIP_ALBUM_ACTION://裁剪返回
                if (data != null) {
                    Bitmap bitmap = data.getParcelableExtra("data");
                    if (bitmap != null) {
                        try {
                            headCimg.setImageBitmap(bitmap);
                            File headFile = new File(SharedPreferencesUtils.getString("CACHE_PATH") + "head.jpg");
                            headFile.deleteOnExit();
                            headFile.createNewFile();
                            FileOutputStream fos = new FileOutputStream(headFile);
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                            fos.flush();
                            fos.close();
                            updateAvatar(headFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            default:
                return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void updateAvatar(File file) {

        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "user/changeAvatar.ac");
        requestParams.setMultipart(true);//上传文件, 使用multipart表单
        requestParams.addHeader("AUTH-TOKEN", SharedPreferencesUtils.getString("AUTH-TOKEN"));
        requestParams.addBodyParameter("imageFile", file);
        x.http().post(requestParams, new Callback.CommonCallback<JSONObject>() {
            @Override
            public void onSuccess(JSONObject result) {
                try {
                    Toast.makeText(x.app(), "头像上传成功", Toast.LENGTH_LONG).show();
                    avatarUrl = result.getString("avatarUrl");
                    user.setAvatarUrl(avatarUrl);
//                    loadAvatar(avatarUrl, headCimg);
                    AvatarEvent avatarEvent = new AvatarEvent(avatarUrl);
                    EventBus.getDefault().post(avatarEvent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                if (ex instanceof ConnectException) {
                    Toast.makeText(x.app(), "头像上传失败:请检查网络环境", Toast.LENGTH_LONG).show();
                }
                if (ex instanceof HttpException) {
                    VrpError vrpError = new Gson().fromJson(((HttpException) ex).getResult(), new TypeToken<VrpError>() {
                    }.getType());
                    Toast.makeText(x.app(), "头像上传失败:" + vrpError.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });

    }

    private void loadAvatar(String imgUrl, final CircleImageView view) {
        ImageOptions imageOptions = new ImageOptions.Builder()
                .setSize(DensityUtil.dip2px(80), DensityUtil.dip2px(80))
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .build();
        x.image().loadDrawable(imgUrl, imageOptions, new Callback.CommonCallback<Drawable>() {

            @Override
            public void onSuccess(Drawable result) {
                view.setImageBitmap(((BitmapDrawable) result).getBitmap());
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

}
