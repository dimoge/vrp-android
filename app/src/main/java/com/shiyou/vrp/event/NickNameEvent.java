package com.shiyou.vrp.event;

/**
 * Created by dime on 16/5/25.
 */
public class NickNameEvent {

    private String name;

    public NickNameEvent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
