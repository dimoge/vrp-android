package com.shiyou.vrp.event;

/**
 * Created by dime on 16/5/25.
 */
public class GenderEvent {

    private String gender;

    public GenderEvent() {
    }

    public GenderEvent(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
