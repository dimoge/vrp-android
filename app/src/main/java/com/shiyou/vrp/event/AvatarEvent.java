package com.shiyou.vrp.event;

/**
 * Created by dime on 16/5/25.
 */
public class AvatarEvent {

    private String avatarUrl;

    public AvatarEvent(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
