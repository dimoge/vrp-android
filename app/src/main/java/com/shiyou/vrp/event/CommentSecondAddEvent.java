package com.shiyou.vrp.event;

import android.widget.LinearLayout;

/**
 * Created by dime on 16/5/30.
 */
public class CommentSecondAddEvent {

    private String cid;//一级评论的id
    private String vid;//评论的视频id
    private String userName;//被回复人的昵称
    private LinearLayout lyt;

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LinearLayout getLyt() {
        return lyt;
    }

    public void setLyt(LinearLayout lyt) {
        this.lyt = lyt;
    }
}
