package com.shiyou.vrp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.shiyou.vrp.R;
import com.shiyou.vrp.entity.Video;

import org.xutils.x;

import java.util.List;

/**
 * Created by dime on 16/6/16.
 */
public class HistoryListAdapter extends BaseAdapter {

    Context context;
    List<Video> videoList;

    public HistoryListAdapter(Context context, List<Video> videoList) {
        this.context = context;
        this.videoList = videoList;
    }

    @Override
    public int getCount() {
        return videoList.size();
    }

    @Override
    public Object getItem(int i) {
        return videoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Holder holder = null;
        if (view == null) {
            holder = new Holder();
            view = LayoutInflater.from(context).inflate(R.layout.item_video_search, viewGroup, false);

            holder.img = (ImageView) view.findViewById(R.id.item_video_search_img);
            holder.titleTv = (TextView) view.findViewById(R.id.item_video_search_title_tv);
            holder.detailTv = (TextView) view.findViewById(R.id.item_video_search_detail_tv);

            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        x.image().bind(holder.img, videoList.get(i).getCoverUrl());
        holder.titleTv.setText(videoList.get(i).getTitle());
        holder.detailTv.setText(videoList.get(i).getDetail());
        return view;
    }

    public class Holder {
        private ImageView img;
        private TextView titleTv;
        private TextView detailTv;
    }

    public void updateData(List<Video> videos) {
        this.videoList = videos;
        notifyDataSetChanged();

    }
}
