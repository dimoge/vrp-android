package com.shiyou.vrp.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.shiyou.vrp.entity.Type;

import java.util.List;

/**
 * Created by dime on 16/4/29.
 */
public class FragmentGreatPagerAdapter extends FragmentPagerAdapter {

    private Context context;
    private List<Fragment> fragmentList;
    private List<Type> typeList;

    public FragmentGreatPagerAdapter(FragmentManager fm, Context context, List<Fragment> fragmentList, List<Type> typeList) {
        super(fm);
        this.context = context;
        this.fragmentList = fragmentList;
        this.typeList = typeList;
    }


    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return typeList.get(position).getTypeName();
    }

    public void updateData(List<Type> types) {
        this.typeList = types;
        this.notifyDataSetChanged();
    }
}
