package com.shiyou.vrp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.activity.LocalListActivity;
import com.shiyou.vrp.entity.Catelogue;
import com.shiyou.vrp.entity.Local;

import org.xutils.DbManager;
import org.xutils.ex.DbException;
import org.xutils.x;

import java.util.List;

/**
 * Created by dime on 16/5/24.
 */
public class LocalCatelogueAdapter extends RecyclerView.Adapter<LocalCatelogueAdapter.CatelogueHolder> {

    private Context context;
    private List<Catelogue> catelogueList;

    private DbManager db;

    public LocalCatelogueAdapter(Context context, List<Catelogue> catelogueList) {
        this.context = context;
        this.catelogueList = catelogueList;
        db = x.getDb(App.getDaoConfig());
    }

    @Override
    public CatelogueHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_catelogue, parent, false);
        CatelogueHolder holder = new CatelogueHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CatelogueHolder holder, final int position) {
        String[] nameSplite = catelogueList.get(position).getName().split("/");
        String name = nameSplite[nameSplite.length-1];
        holder.nameTv.setText(name);
        try {
            long count = db.selector(Local.class).where("parent", "=", catelogueList.get(position).getName()).count();
            holder.countTv.setText(count+"");
        } catch (DbException e) {
            e.printStackTrace();
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LocalListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                intent.putExtra("PARENT", catelogueList.get(position).getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return catelogueList.size();
    }

    public class CatelogueHolder extends RecyclerView.ViewHolder{

        private TextView nameTv;
        private TextView countTv;

        public CatelogueHolder(View itemView) {
            super(itemView);

            nameTv = (TextView) itemView.findViewById(R.id.item_catelogue_name_tv);
            countTv = (TextView) itemView.findViewById(R.id.item_catelogue_count_tv);
        }
    }

    public void updateDate(List<Catelogue> catelogueList){
        this.catelogueList = catelogueList;
        this.notifyDataSetChanged();
    }
}
