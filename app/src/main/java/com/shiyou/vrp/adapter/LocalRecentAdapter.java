package com.shiyou.vrp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.activity.LocalPlayerActivity;
import com.shiyou.vrp.activity.PlayerActivity;
import com.shiyou.vrp.entity.Local;

import org.xutils.ex.DbException;
import org.xutils.x;

import java.util.Date;
import java.util.List;

/**
 * Created by dime on 2016/5/18 0018.
 */
public class LocalRecentAdapter extends RecyclerView.Adapter<LocalRecentAdapter.LocalHolder> {

    private Context context;
    private List<Local> localList;

    public LocalRecentAdapter(Context context, List<Local> localList) {
        this.context = context;
        this.localList = localList;
    }

    @Override
    public LocalHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_local, parent, false);
        LocalHolder localHolder = new LocalHolder(view);
        return localHolder;
    }

    @Override
    public void onBindViewHolder(LocalHolder holder, final int position) {
        holder.nameTv.setText(localList.get(position).getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Local save = localList.get(position);
                save.setDate(new Date());
                try {
                    x.getDb(App.getDaoConfig()).save(save);
                } catch (DbException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(context, LocalPlayerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                Bundle bundle = new Bundle();
                bundle.putSerializable("LOCAL", localList.get(position));
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return localList.size();
    }

    public class LocalHolder extends RecyclerView.ViewHolder {

        private TextView nameTv;

        public LocalHolder(View itemView) {
            super(itemView);
            nameTv = (TextView) itemView.findViewById(R.id.item_local_name_tv);
        }
    }

    public void updateData(List<Local> ll) {
        this.localList = ll;
        notifyDataSetChanged();
    }
}
