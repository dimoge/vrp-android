package com.shiyou.vrp.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.shiyou.vrp.entity.Spread;

import org.xutils.x;

import java.util.List;

/**
 * Created by dime on 16/5/10.
 */
public class FragmentTypeOneSpreadAdapter extends PagerAdapter {

    private Context context;
    private List<ImageView> imageViewList;
    private List<Spread> spreadList;

    public FragmentTypeOneSpreadAdapter(Context context, List<ImageView> imageViewList, List<Spread> spreadList) {
        this.context = context;
        this.imageViewList = imageViewList;
        this.spreadList = spreadList;
    }

    @Override
    public int getCount() {
        return imageViewList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        x.image().bind(imageViewList.get(position), spreadList.get(position).getImg());
        container.removeView(imageViewList.get(position));
        container.addView(imageViewList.get(position));
        return imageViewList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
