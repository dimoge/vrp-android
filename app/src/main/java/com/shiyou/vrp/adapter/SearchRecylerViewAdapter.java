package com.shiyou.vrp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shiyou.vrp.R;
import com.shiyou.vrp.activity.PlayerActivity;
import com.shiyou.vrp.entity.Video;

import org.xutils.x;

import java.util.List;

/**
 * Created by dime on 16/5/10.
 */
public class SearchRecylerViewAdapter extends RecyclerView.Adapter<SearchRecylerViewAdapter.SearchHolder> {

    private Context context;
    private List<Video> videoList;

    public SearchRecylerViewAdapter(Context context, List<Video> videoList) {
        this.context = context;
        this.videoList = videoList;
    }

    @Override
    public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_video_search, parent, false);
        SearchHolder searchHolder = new SearchHolder(view);

        return searchHolder;
    }

    @Override
    public void onBindViewHolder(SearchHolder holder, final int position) {
        holder.titleTv.setText(videoList.get(position).getTitle());
        holder.detailTv.setText(videoList.get(position).getDetail());
        x.image().bind(holder.img, videoList.get(position).getCoverUrl());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PlayerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("VIDEO", videoList.get(position));
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public class SearchHolder extends RecyclerView.ViewHolder {

        private ImageView img;
        private TextView titleTv;
        private TextView detailTv;

        public SearchHolder(View itemView) {
            super(itemView);

            img = (ImageView) itemView.findViewById(R.id.item_video_search_img);
            titleTv = (TextView) itemView.findViewById(R.id.item_video_search_title_tv);
            detailTv = (TextView) itemView.findViewById(R.id.item_video_search_detail_tv);

        }
    }
}
