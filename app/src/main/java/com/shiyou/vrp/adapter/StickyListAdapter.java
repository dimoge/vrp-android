package com.shiyou.vrp.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.entity.Comment;
import com.shiyou.vrp.entity.Video;
import com.shiyou.vrp.utils.HttpUtils;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by dime on 2016/5/26 0026.
 */
public class StickyListAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private Context context;
    private List<Comment> commentList;
    private Video video;

    private LayoutInflater inflater;
    private Gson gson = new Gson();

    public StickyListAdapter(Context context, List<Comment> commentList, Video video) {
        this.context = context;
        this.commentList = commentList;
        this.video = video;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_comment_input, parent, false);
            holder = new HeaderViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_comment, parent, false);
            holder = new ViewHolder(convertView);
            //初始化数据
            Comment temp = commentList.get(position);
            holder.nickNameTv.setText(temp.getUserName());
            holder.contentTv.setText(temp.getContent());
            holder.dateTv.setText(temp.getCreate_time());
            HttpUtils.loadCimg(temp.getAvatar(), holder.avatarImg);
            //初始化二级评论
            getSecondHttp(temp.getId(), holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return 0;
    }

    @Override
    public int getCount() {
        return commentList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class HeaderViewHolder {
        CircleImageView headImg;
        EditText contentEdit;
        Button doneBtn;

        public HeaderViewHolder(View view) {
            headImg = (CircleImageView) view.findViewById(R.id.item_comment_input_cimg);
            contentEdit = (EditText) view.findViewById(R.id.item_comment_input_content_edit);
            doneBtn = (Button) view.findViewById(R.id.item_comment_input_btn);
        }
    }

    class ViewHolder {
        CircleImageView avatarImg;
        TextView nickNameTv;
        TextView contentTv;
        TextView dateTv;
        TextView replyTv;
        LinearLayout secondLyt;

        public ViewHolder(View view) {
            avatarImg = (CircleImageView) view.findViewById(R.id.item_comment_avatar_cimg);
            nickNameTv = (TextView) view.findViewById(R.id.item_comment_nikename_tv);
            contentTv = (TextView) view.findViewById(R.id.item_comment_content_tv);
            dateTv = (TextView) view.findViewById(R.id.item_comment_date_tv);
            replyTv = (TextView) view.findViewById(R.id.item_comment_reply_tv);
        }
    }

    private void getSecondHttp(String id, final ViewHolder holder) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "secondComment/getSecondComment.ac");
        requestParams.addBodyParameter("cid", id);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                List<Comment> secondComList = gson.fromJson(result, new TypeToken<List<Comment>>() {
                }.getType());
                for (Comment secondComTemp : secondComList) {
                    View view = inflater.inflate(R.layout.item_comment, null, true);
                    ViewHolder secondHolder = new ViewHolder(view);
                    secondHolder.nickNameTv.setText(secondComTemp.getUserName());
                    secondHolder.contentTv.setText(secondComTemp.getContent());
                    secondHolder.dateTv.setText(secondComTemp.getCreate_time());
                    secondHolder.replyTv.setVisibility(View.GONE);
                    HttpUtils.loadCimg(secondComTemp.getAvatar(), secondHolder.avatarImg);
                    holder.secondLyt.addView(view);
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

}
