package com.shiyou.vrp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shiyou.vrp.App;
import com.shiyou.vrp.R;
import com.shiyou.vrp.entity.Comment;
import com.shiyou.vrp.entity.Video;
import com.shiyou.vrp.event.CommentSecondAddEvent;
import com.shiyou.vrp.utils.HttpUtils;

import org.greenrobot.eventbus.EventBus;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dime on 16/5/29.
 */
public class CommentRecylerAdapter extends RecyclerView.Adapter<CommentRecylerAdapter.CommentHolder> {

    private Context context;
    private List<Comment> commentList;
    private Video video;

    private LayoutInflater inflater;
    private Gson gson = new Gson();

    public CommentRecylerAdapter(Context context, List<Comment> commentList, Video video) {
        this.context = context;
        this.commentList = commentList;
        this.video = video;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_comment, parent, false);
        CommentHolder holder = new CommentHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final CommentHolder holder, int position) {
        final Comment comment = commentList.get(position);
        HttpUtils.loadCimg(comment.getAvatar(), holder.avatarImg);
        holder.nickNameTv.setText(comment.getUserName());
        holder.contentTv.setText(comment.getContent());
        holder.dateTv.setText(comment.getCreate_time());
        //初始化二级评论数据
        getSecond3Http(holder.lyt, comment.getId());
        //添加二级评论数据
        holder.replyTv.setOnClickListener(new View.OnClickListener() {
            LinearLayout lyt = holder.lyt;
            Comment temp = comment;

            @Override
            public void onClick(View view) {
                CommentSecondAddEvent commentSecondAddEvent = new CommentSecondAddEvent();
                commentSecondAddEvent.setCid(comment.getId());
                commentSecondAddEvent.setUserName(comment.getUserName());
                commentSecondAddEvent.setLyt(lyt);
                EventBus.getDefault().post(commentSecondAddEvent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class CommentHolder extends RecyclerView.ViewHolder {

        CircleImageView avatarImg;
        TextView nickNameTv;
        TextView contentTv;
        TextView dateTv;
        TextView replyTv;
        LinearLayout lyt;

        public CommentHolder(View itemView) {
            super(itemView);

            avatarImg = (CircleImageView) itemView.findViewById(R.id.item_comment_avatar_cimg);
            nickNameTv = (TextView) itemView.findViewById(R.id.item_comment_nikename_tv);
            contentTv = (TextView) itemView.findViewById(R.id.item_comment_content_tv);
            dateTv = (TextView) itemView.findViewById(R.id.item_comment_date_tv);
            replyTv = (TextView) itemView.findViewById(R.id.item_comment_reply_tv);
            lyt = (LinearLayout) itemView.findViewById(R.id.item_comment_lyt);
        }
    }

    public class CommentSecondHolder {

        public CircleImageView headImg;
        public TextView nickNameTv;
        public TextView contentTv;
        public TextView dateTv;

        public CommentSecondHolder(View view) {

            headImg = (CircleImageView) view.findViewById(R.id.item_comment_second_avatar_cimg);
            nickNameTv = (TextView) view.findViewById(R.id.item_comment_second_nikename_tv);
            contentTv = (TextView) view.findViewById(R.id.item_comment_second_content_tv);
            dateTv = (TextView) view.findViewById(R.id.item_comment_second_date_tv);
        }
    }

    /**
     * 刷新数据
     *
     * @param commentList
     */
    public void updateData(List<Comment> commentList) {
        this.commentList = commentList;
        notifyDataSetChanged();
    }

    private void addCommentSecondHttp(String content, String vid, String authToken) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "comment/add.ac");
        requestParams.addHeader(App.AUTH_TOKEN, authToken);
        requestParams.addBodyParameter("content", content);
        requestParams.addBodyParameter("vid", video.getId());
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(context, "添加一级评论成功" + result, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });

    }


    /**
     * 获取前三条 二级评论
     *
     * @param lyt
     * @param id
     */
    private void getSecond3Http(final LinearLayout lyt, final String id) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "secondComment/getThreeSecondComment.ac");
        requestParams.addBodyParameter("cid", id);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                //显示二级评论>>>>>>>前三条
                List<Comment> commentSecondList = gson.fromJson(result, new TypeToken<List<Comment>>() {
                }.getType());
                for (Comment commentSecond : commentSecondList) {
                    View view = inflater.inflate(R.layout.item_comment_second, null, true);
                    CommentSecondHolder holder = new CommentSecondHolder(view);
                    HttpUtils.loadCimg(commentSecond.getAvatar(), holder.headImg);
                    holder.nickNameTv.setText(commentSecond.getUserName());
                    holder.contentTv.setText(commentSecond.getContent());
                    holder.dateTv.setText(commentSecond.getCreate_time());
                    lyt.addView(view);
                }
                if (commentSecondList.size() == 3) {
                    final TextView moreTv = new TextView(context);
                    moreTv.setText("更多评论");
                    moreTv.setTextColor(Color.parseColor("#0066FF"));
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    moreTv.setLayoutParams(params);
                    lyt.addView(moreTv);
                    moreTv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            moreTv.setText("正在加载更多评论....");
                            getSecondHttp(lyt, id);
                        }
                    });
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 获取所有二级评论
     *
     * @param id
     */
    private void getSecondHttp(final LinearLayout lyt, String id) {
        RequestParams requestParams = new RequestParams(App.SERVER_URL + App.API_ROOT + "secondComment/getSecondComment.ac");
        requestParams.addBodyParameter("cid", id);
        x.http().post(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                //显示所有二级评论
                List<Comment> commentSecondList = gson.fromJson(result, new TypeToken<List<Comment>>() {
                }.getType());
                lyt.removeAllViews();
                for (Comment commentSecond : commentSecondList) {
                    View view = inflater.inflate(R.layout.item_comment_second, null, true);
                    CommentSecondHolder holder = new CommentSecondHolder(view);
                    HttpUtils.loadCimg(commentSecond.getAvatar(), holder.headImg);
                    holder.nickNameTv.setText(commentSecond.getUserName());
                    holder.contentTv.setText(commentSecond.getContent());
                    holder.dateTv.setText(commentSecond.getCreate_time());
                    lyt.addView(view);
                }

            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }
}
