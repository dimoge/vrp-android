package com.shiyou.vrp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.shiyou.vrp.R;
import com.shiyou.vrp.entity.Video;
import com.shiyou.vrp.activity.PlayerActivity;

import org.xutils.x;

import java.util.List;

/**
 * Created by dime on 16/5/9.
 */
public class FragmentTypeCommentRecylerViewAdapter extends RecyclerView.Adapter<FragmentTypeCommentRecylerViewAdapter.CommentHolder> {

    private Context context;
    private List<Video> videoList;

    public FragmentTypeCommentRecylerViewAdapter(Context context, List<Video> videoList) {
        this.context = context;
        this.videoList = videoList;
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_video, parent, false);
        CommentHolder commentHolder = new CommentHolder(view);
        return commentHolder;
    }

    @Override
    public void onBindViewHolder(FragmentTypeCommentRecylerViewAdapter.CommentHolder holder, final int position) {
        holder.titleTv.setText(videoList.get(position).getTitle());
        holder.detailTv.setText(videoList.get(position).getDetail());
        x.image().bind(holder.imageView, videoList.get(position).getCoverUrl() );
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PlayerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("VIDEO", videoList.get(position));
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public static class CommentHolder extends RecyclerView.ViewHolder{


        public ImageView imageView;
        public TextView titleTv;
        public TextView detailTv;

        public CommentHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.item_video_img);
            titleTv = (TextView) itemView.findViewById(R.id.item_video_title_tv);
            detailTv = (TextView) itemView.findViewById(R.id.item_video_detail_tv);
        }
    }
}
