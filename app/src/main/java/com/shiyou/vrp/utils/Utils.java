package com.shiyou.vrp.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.AnimationDrawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.shiyou.vrp.download.DownloadState;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by xilin on 2015/12/14.
 */
public class Utils {
    public static String getShowTime(long milliseconds) {
        // 获取日历函数
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        SimpleDateFormat dateFormat = null;
        // 判断是否大于60分钟，如果大于就显示小时。设置日期格式
        if (milliseconds / 60000 > 60) {
            dateFormat = new SimpleDateFormat("hh:mm:ss");
        } else {
            dateFormat = new SimpleDateFormat("00:mm:ss");
        }
        return dateFormat.format(calendar.getTime());
    }

    public static void startImageAnim(ImageView Img, int anim) {
        Img.setVisibility(View.VISIBLE);
        try {
            Img.setImageResource(anim);
            AnimationDrawable animationDrawable = (AnimationDrawable) Img.getDrawable();
            animationDrawable.start();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    public static void stopImageAnim(ImageView Img) {
        try {
            AnimationDrawable animationDrawable = (AnimationDrawable) Img.getDrawable();
            animationDrawable.stop();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        Img.setVisibility(View.GONE);
    }

    /**
     * 判断activity是竖屏吗
     *
     * @param context return true/false
     */
    public static boolean isScreenPortrait(Context context) {
        return context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT ? true : false;
    }

    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    public static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 获取媒体类型
     *
     * @param path
     * @return

    public static UVMediaType getMediaType(String path) {
        if (path.endsWith(".mp4")) {
            return UVMediaType.UVMEDIA_TYPE_MP4;
        } else if (path.endsWith(".m3u8")) {
            return UVMediaType.UVMEDIA_TYPE_M3U8;
        } else {
            return null;
        }
    }
     */


    /**
     * 将时间转为字符串
     *
     * @param date
     * @return
     */
    public static String date2String(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    /**
     * 将long类型转为两位小数的M单位
     *
     * @param number
     * @return
     */
    public static String longFormat(long number) {
        DecimalFormat df = new DecimalFormat("###.00");
        double m = (double) number / 1024 / 1024;
        Log.e("what", m + "");
        return df.format(m);
    }


    /**
     * 将DownloadState枚举类型转为字符串
     *
     * @param downloadState
     * @return
     */
    public static String downloadState2String(DownloadState downloadState) {
        switch (downloadState.value()) {
            case 0:
                return "等待";
            case 1:
                return "开始";
            case 2:
                return "播放";
            case 3:
                return "停止";
            case 4:
                return "错误";
            default:
                return "停止";
        }
    }

}
