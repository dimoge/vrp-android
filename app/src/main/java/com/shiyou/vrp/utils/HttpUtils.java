package com.shiyou.vrp.utils;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import org.xutils.common.Callback;
import org.xutils.common.util.DensityUtil;
import org.xutils.image.ImageOptions;
import org.xutils.x;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by dime on 2016/5/27 0027.
 */
public class HttpUtils {

    /**
     * 异步加载头像
     * @param avatarUrl
     * @param view
     */
    public static void loadCimg(String avatarUrl, final CircleImageView view) {
        ImageOptions imageOptions = new ImageOptions.Builder()
                .setSize(DensityUtil.dip2px(80), DensityUtil.dip2px(80))
                .setImageScaleType(ImageView.ScaleType.CENTER_CROP)
                .build();
        x.image().loadDrawable(avatarUrl, imageOptions, new Callback.CommonCallback<Drawable>() {
            @Override
            public void onSuccess(Drawable result) {
                view.setImageBitmap(((BitmapDrawable) result).getBitmap());
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

}
