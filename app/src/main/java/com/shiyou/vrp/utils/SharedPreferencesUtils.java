package com.shiyou.vrp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.x;

/**
 * Created by dime on 2016/5/23 0023.
 */
public class SharedPreferencesUtils {

    private static final String TAG = "vrp-data";

    //初始化配置文件实例
    private static SharedPreferences sharedPreferences = x.app().getSharedPreferences(TAG, Context.MODE_PRIVATE);

    //初始化编辑器
    private static SharedPreferences.Editor editor = sharedPreferences.edit();

    //私有化构造函数,防止new
    private SharedPreferencesUtils() {

    }

    public static boolean contains(String key) {
        return sharedPreferences.contains(key);
    }

    /*
        Set方法
     */

    /**
     * 设置布尔值
     *
     * @param key   键
     * @param value 值
     */
    public static void set(String key, boolean value) {
        editor.putBoolean(key, value).commit();
    }

    /**
     * 设置字符串
     *
     * @param key   键
     * @param value 值
     */
    public static void set(String key, String value) {
        editor.putString(key, value).commit();
    }

    /**
     * 设置整型
     *
     * @param key   键
     * @param value 值
     */
    public static void set(String key, int value) {
        editor.putInt(key, value).commit();
    }

    /**
     * 设置浮点数
     *
     * @param key   键
     * @param value 值
     */
    public static void set(String key, float value) {
        editor.putFloat(key, value).commit();
    }

    /**
     * 设置json对象,本质上是将json序列化为string然后存入
     *
     * @param key        键
     * @param jsonObject 值
     */
    public static void set(String key, JSONObject jsonObject) {
        set(key, jsonObject.toString());
    }

    /*
        Get方法
     */

    /**
     * 获取布尔值
     *
     * @param key 键
     * @return 对应的值, 默认为true
     */
    public static boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, true);
    }

    /**
     * 获取字符串
     *
     * @param key 键
     * @return 对应的值, 默认是"", TODO:为什么不是""?
     */
    public static String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    /**
     * 获取整型
     *
     * @param key 键
     * @return 对应的值, 默认是0
     */
    public static int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    /**
     * 获取浮点数
     *
     * @param key 键
     * @return 对应的值, 默认是0.0
     */
    public static float getFloat(String key) {
        return sharedPreferences.getFloat(key, 0f);
    }

    /**
     * 获取json对象
     *
     * @param key 键
     * @return 对应的值, 本质上是将string格式化为json对象, 可能会异常, 异常直接返回空的json对象
     */
    public static JSONObject getJson(String key) {
        try {
            return new JSONObject(getString(key));
        } catch (JSONException e) {
            return new JSONObject();
        }
    }
}
